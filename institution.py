# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

try:
    from PIL import Image
except ImportError:
    Image = None

from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pyson import Eval, Not, Bool, And, Equal, Or


class HealthInstitution(ModelSQL, ModelView):
    'Health Institution'
    __name__ = 'health.institution'
    party = fields.Many2One('party.party', 'Party', required=True,
        domain=[('is_institution', '=', True)],
        help='Party Associated to this Health Institution',
        states={'readonly': Bool(Eval('party'))})
    code = fields.Char('Code', required=True, help="Institution code")
    picture = fields.Binary('Picture')
    institution_type = fields.Selection((
        (None, ''),
        ('doctor_office', 'Doctor office'),
        ('primary_care', 'Primary Care Center'),
        ('clinic', 'Clinic'),
        ('hospital', 'General Hospital'),
        ('specialized', 'Specialized Hospital'),
        ('nursing_home', 'Nursing Home'),
        ('hospice', 'Hospice'),
        ('rural', 'Rural facility'),
        ), 'Type', required=True, sort=False)
    beds = fields.Integer("Beds")
    operating_room = fields.Boolean("Operating Room",
        help="Check this box if the institution has operating rooms",
    )
    or_number = fields.Integer("ORs",
        states={'invisible': Not(Bool(Eval('operating_room')))})
    public_level = fields.Selection((
        (None, ''),
        ('private', 'Private'),
        ('public', 'Public'),
        ('mixed', 'Private - State'),
        ), 'Public Level', required=True, sort=False)
    teaching = fields.Boolean("Teaching", help="Mark if this is a teaching institution")
    heliport = fields.Boolean("Heliport")
    trauma_center = fields.Boolean("Trauma Center")
    trauma_level = fields.Selection((
        (None, ''),
        ('one', 'Level I'),
        ('two', 'Level II'),
        ('three', 'Level III'),
        ('four', 'Level IV'),
        ('five', 'Level V'),
        ), 'Trauma Level', sort=False,
        states={'invisible': Not(Bool(Eval('trauma_center')))})
    extra_info = fields.Text("Extra Info")
    # Add Specialties to the Health Institution
    specialties = fields.One2Many('health.institution.specialties',
        'institution', 'Specialties',
        help="Specialties Provided in this Health Institution")
    main_specialty = fields.Many2One('health.institution.specialties',
        'Specialty',
        domain=[('institution', '=', Eval('id'))],
        depends=['specialties', 'institution_type', 'id'],
        help="Choose the speciality in the case of Specialized Hospitals" \
            " or where this center excels",
        states={'required': And(Eval('institution_type') == 'specialized',
            Eval('id', 0) > 0),
            'readonly': Eval('id', 0) < 0})
    # Add Specialties to the Health Institution
    operational_sectors = fields.One2Many(
        'health.institution.operationalsector', 'institution',
        'Operational Sector',
        help="Operational Sectors covered by this institution")

    @classmethod
    def __setup__(cls):
        super(HealthInstitution, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.party), 'This Institution already exists !'),
            ('code_uniq', Unique(t, t.code), 'This CODE already exists !'),
        ]

    def get_rec_name(self, name):
        if self.party:
            return self.party.name

    @classmethod
    def get_institution(cls):
        # FIXME: Add domain
        institutions = cls.search([])
        if institutions:
            return institutions[0].id


class HealthInstitutionSpecialties(ModelSQL, ModelView):
    'Health Institution Specialties'
    __name__ = 'health.institution.specialties'
    institution = fields.Many2One('health.institution', 'Institution',
        required=True)
    specialty = fields.Many2One('health.specialty', 'Specialty', required=True)

    def get_rec_name(self, name):
        if self.specialty:
            return self.specialty.name

    @classmethod
    def __setup__(cls):
        super(HealthInstitutionSpecialties, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_sp_uniq', Unique(t, t.institution, t.specialty),
                'The Specialty already exists for this institution'),
        ]


class HealthInstitutionOperationalSector(ModelSQL, ModelView):
    'Operational Sectors covered by Institution'
    __name__ = 'health.institution.operationalsector'
    institution = fields.Many2One('health.institution', 'Institution',
        required=True)
    operational_sector = fields.Many2One('health.operational_sector',
        'Operational Sector', required=True)

    @classmethod
    def __setup__(cls):
        super(HealthInstitutionOperationalSector, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_os_uniq', Unique(t, t.institution, t.operational_sector),
                'The Operational Sector already exists for this institution'),
        ]


class HospitalBuilding(ModelSQL, ModelView):
    'Hospital Building'
    __name__ = 'health.hospital.building'
    name = fields.Char('Name', required=True,
        help='Name of the building within the institution')
    institution = fields.Many2One('health.institution', 'Institution',
        required=True, help='Health Institution of this building')
    code = fields.Char('Code', required=True)
    extra_info = fields.Text('Extra Info')

    @classmethod
    def __setup__(cls):
        super(HospitalBuilding, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name, t.institution),
                'The Building name must be unique per Health'
                ' Center'),
            ('code_uniq', Unique(t, t.code, t.institution),
                'The Building code must be unique per Health'
                ' Center'),
        ]


class HospitalUnit(ModelSQL, ModelView):
    'Hospital Unit'
    __name__ = 'health.hospital.unit'
    name = fields.Char('Name', required=True,
        help='Name of the unit, eg Neonatal, Intensive Care, ...')
    institution = fields.Many2One(
        'health.institution', 'Institution', required=True)
    code = fields.Char('Code', required=True)
    extra_info = fields.Text('Extra Info')

    @classmethod
    def __setup__(cls):
        super(HospitalUnit, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name, t.institution),
                'The Unit NAME must be unique per Health'
                ' Center'),
            ('code_uniq', Unique(t, t.code, t.institution),
                'The Unit CODE must be unique per Health'
                ' Center'),
        ]


class HospitalOR(ModelSQL, ModelView):
    'Operating Room'
    __name__ = 'health.hospital.or'

    name = fields.Char('Name', required=True, help='Name of the Operating Room')
    institution = fields.Many2One('health.institution', 'Institution',
        required=True, help='Health Institution')
    building = fields.Many2One(
        'health.hospital.building', 'Building',
        domain=[('institution', '=', Eval('institution'))],
        depends=['institution'],
        select=True)
    unit = fields.Many2One('health.hospital.unit', 'Unit',
        domain=[('institution', '=', Eval('institution'))],
        depends=['institution'])
    extra_info = fields.Text('Extra Info')
    state = fields.Selection((
        (None, ''),
        ('free', 'Free'),
        ('confirmed', 'Confirmed'),
        ('occupied', 'Occupied'),
        ('na', 'Not available'),
        ), 'Status', readonly=True, sort=False)

    @staticmethod
    def default_state():
        return 'free'

    @classmethod
    def __setup__(cls):
        super(HospitalOR, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name, t.institution),
                'The Operating Room Name must be unique per Health'
                ' Center'),
        ]


class HospitalWard(ModelSQL, ModelView):
    'Hospital Ward'
    __name__ = 'health.hospital.ward'
    name = fields.Char('Name', required=True, help='Ward / Room code')
    institution = fields.Many2One(
        'health.institution', 'Institution', required=True,
        help='Health Institution')
    building = fields.Many2One('health.hospital.building', 'Building',
        domain=[('institution', '=', Eval('institution'))],
        depends=['institution'])
    floor = fields.Integer('Floor Number')
    unit = fields.Many2One('health.hospital.unit', 'Unit',
        domain=[('institution', '=', Eval('institution'))],
        depends=['institution'])
    private = fields.Boolean(
        'Private', help='Check this option for private room')
    bio_hazard = fields.Boolean(
        'Bio Hazard', help='Check this option if there is biological hazard')
    number_of_beds = fields.Integer(
        'Number of beds', help='Number of patients per ward')
    telephone = fields.Boolean('Telephone access')
    ac = fields.Boolean('Air Conditioning')
    private_bathroom = fields.Boolean('Private Bathroom')
    guest_sofa = fields.Boolean('Guest sofa-bed')
    tv = fields.Boolean('Television')
    internet = fields.Boolean('Internet Access')
    refrigerator = fields.Boolean('Refrigerator')
    microwave = fields.Boolean('Microwave')
    gender = fields.Selection((
        (None, ''),
        ('men', 'Men Ward'),
        ('women', 'Women Ward'),
        ('unisex', 'Unisex'),
        ), 'Gender', required=True, sort=False)
    state = fields.Selection((
        (None, ''),
        ('beds_available', 'Beds available'),
        ('full', 'Full'),
        ('na', 'Not available'),
        ), 'Status', sort=False)
    extra_info = fields.Text('Extra Info')

    @staticmethod
    def default_gender():
        return 'unisex'

    @staticmethod
    def default_number_of_beds():
        return 1

    @classmethod
    def __setup__(cls):
        super(HospitalWard, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name, t.institution),
                'The Ward / Room Name must be unique per Health'
                ' Center'),
        ]


class HospitalBed(ModelSQL, ModelView):
    'Hospital Bed'
    __name__ = 'health.hospital.bed'
    _rec_name = 'telephone_number'
    product = fields.Many2One('product.product', 'Bed', required=True,
        domain=[('kind', '=', 'bed')], help='Bed Number')
    institution = fields.Many2One('health.institution', 'Institution',
        required=True, help='Health Institution')
    ward = fields.Many2One(
        'health.hospital.ward', 'Ward',
        domain=[('institution', '=', Eval('institution'))],
        depends=['institution'],
        help='Ward or room')
    bed_type = fields.Selection((
        (None, ''),
        ('gatch', 'Gatch Bed'),
        ('electric', 'Electric'),
        ('stretcher', 'Stretcher'),
        ('low', 'Low Bed'),
        ('low_air_loss', 'Low Air Loss'),
        ('circo_electric', 'Circo Electric'),
        ('clinitron', 'Clinitron'),
        ), 'Bed Type', required=True, sort=False)
    telephone_number = fields.Char(
        'Telephone Number', help='Telephone number / Extension')
    extra_info = fields.Text('Extra Info')
    state = fields.Selection((
        (None, ''),
        ('free', 'Free'),
        ('reserved', 'Reserved'),
        ('occupied', 'Occupied'),
        ('to_clean', 'Needs cleaning'),
        ('na', 'Not available'),
        ), 'Status', readonly=True, sort=False)

    @staticmethod
    def default_bed_type():
        return 'gatch'

    @staticmethod
    def default_state():
        return 'free'

    def get_rec_name(self, name):
        if self.product:
            return self.product.name

    @classmethod
    def search_rec_name(cls, name, clause):
        return [('product',) + tuple(clause[1:])]

    @classmethod
    def __setup__(cls):
        super(HospitalBed, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.product, t.institution),
                'The Bed must be unique per Health Center'),
        ]
        # Show fix button when is in state "needs cleaning" or "NA"
        cls._buttons.update({
            'fix_bed': {
                'invisible': Or(
                    Equal(Eval('state'), 'free'),
                    Equal(Eval('state'), 'occupied'),
                    Equal(Eval('state'), 'reserved')
                )
            },
        }),

    @classmethod
    @ModelView.button
    def fix_bed(cls, beds):
        cls.write(beds, {'state': 'free'})
