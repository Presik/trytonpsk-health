
from trytond.pool import Pool
from .wizard import (
    wizard_appointment_evaluation,
    wizard_check_immunization_status
)
from .report import health_report, immunization_status_report
from . import health
from . import user
from . import configuration
from . import party
from . import institution
from . import product
from . import evaluation


def register():
    Pool.register(
        configuration.Configuration,
        product.Product,
        product.ProductTemplate,
        health.OperationalArea,
        health.OperationalSector,
        health.DomiciliaryUnit,
        health.Occupation,
        health.DeathCertificate,
        health.BirthCertificate,
        party.Ethnicity,
        party.Party,
        party.ContactMechanism,
        party.PersonName,
        party.PartyAddress,
        party.AlternativePersonID,
        institution.HealthInstitution,
        institution.HealthInstitutionSpecialties,
        institution.HealthInstitutionOperationalSector,
        # institution.HealthInstitutionO2M,
        health.DrugDoseUnits,
        health.MedicationFrequency,
        health.DrugForm,
        health.DrugRoute,
        health.MedicalSpecialty,
        institution.HospitalBuilding,
        institution.HospitalUnit,
        institution.HospitalOR,
        institution.HospitalWard,
        institution.HospitalBed,
        health.HealthProfessional,
        health.HealthProfessionalSpecialties,
        health.PhysicianSP,
        health.Family,
        health.FamilyMember,
        health.MedicamentCategory,
        health.Medicament,
        health.ImmunizationSchedule,
        health.ImmunizationScheduleLine,
        health.ImmunizationScheduleDose,
        health.PathologyCategory,
        health.PathologyGroup,
        health.Pathology,
        health.DiseaseMembers,
        health.BirthCertExtraInfo,
        health.DeathCertExtraInfo,
        health.DeathUnderlyingCondition,
        health.InsurancePlan,
        health.ProcedureCode,
        health.Insurance,
        health.PatientData,
        health.PatientDiseaseInfo,
        health.Appointment,
        health.AppointmentReport,
        health.OpenAppointmentReportStart,
        health.PatientPrescriptionOrder,
        health.PrescriptionLine,
        health.PatientMedication,
        health.PatientVaccination,
        health.PatientECG,
        # health.CheckImmunizationStatusInit,
        user.User,
        evaluation.PatientEvaluation,
        evaluation.Obstetrics,
        evaluation.Directions,
        evaluation.SecondaryCondition,
        evaluation.DiagnosticHypothesis,
        evaluation.SignsAndSymptoms,
        evaluation.Nutrition,
        evaluation.EvaluationMental,
        module='health', type_='model')
    Pool.register(
        wizard_appointment_evaluation.CreateAppointmentEvaluation,
        wizard_check_immunization_status.CheckImmunizationStatus,
        # OpenAppointmentReport,
        module='health', type_='wizard')
    Pool.register(
        health_report.PatientDiseaseReport,
        health_report.PatientMedicationReport,
        health_report.PatientVaccinationReport,
        immunization_status_report.ImmunizationStatusReport,
        module='health', type_='report')
