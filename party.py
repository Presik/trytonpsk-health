# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import datetime, date
from io import BytesIO
try:
    from PIL import Image
except ImportError:
    Image = None

from trytond.i18n import gettext
from trytond.pool import Pool
from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pyson import Not, Bool, Eval
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.exceptions import UserError


class Party(ModelSQL, ModelView):
    __name__ = 'party.party'
    person_names = fields.One2Many('health.person_name', 'party',
        'Person Names', states={'invisible': Not(Bool(Eval('is_person')))})
    name_representation = fields.Selection([
        (None, ''),
        ('pgfs', 'Prefix Given Family, Suffix'),
        ('gf', 'Given Family'),
        ('fg', 'Family, Given'),
        ], 'Name Representation',
        states={'invisible': Not(Bool(Eval('is_person')))})
    activation_date = fields.Date(
        'Activation date', help='Date of activation of the party')
    puid = fields.Char('PUID', help='Person Unique Identifier',
        states={
            'invisible': Not(Bool(Eval('is_person'))),
            'readonly': True,
        })
    unidentified = fields.Boolean(
        'Unidentified',
        help='Patient is currently unidentified',
        states={'invisible': Not(Bool(Eval('is_person')))})
    is_person = fields.Boolean(
        'Person',
        help='Check if the party is a person.')
    is_patient = fields.Boolean(
        'Patient',
        states={'invisible': Not(Bool(Eval('is_person')))},
        help='Check if the party is a patient')
    is_healthprof = fields.Boolean(
        'Health Prof',
        states={'invisible': Not(Bool(Eval('is_person')))},
        help='Check if the party is a health professional')
    is_institution = fields.Boolean(
        'Institution', help='Check if the party is a Health Care Institution',
        states={
            'invisible': Eval('is_person', False)
        })
    is_insurance_company = fields.Boolean(
        'Insurance Company', help='Check if the party is an Insurance Company',
        states={
            'invisible': Eval('is_person', False)
        })
    is_pharmacy = fields.Boolean(
        'Pharmacy', help='Check if the party is a Pharmacy',
        states={
            'invisible': Eval('is_person', False)
        })
    photo = fields.Binary('Picture')
    ethnic_group = fields.Many2One('health.ethnicity', 'Ethnicity')
    residence = fields.Many2One(
        'country.country', 'Residence', help='Country of Residence')
    insurance = fields.One2Many('health.insurance', 'party', 'Insurances',
        help="Insurance Plans associated to this party")
    internal_user = fields.Many2One('res.user', 'Internal User', help="""
            In GNU Health is the user (person) that logins. When the'
            party is a person, it will be the user'
            that maps the party.
        """, states={
            'invisible': Not(Bool(Eval('is_person'))),
        }
    )
    insurance_company_type = fields.Selection([
        (None, ''),
        ('state', 'State'),
        ('labour_union', 'Labour Union / Syndical'),
        ('private', 'Private'),
        ], 'Insurance Type', select=True)
    insurance_plans = fields.One2Many('health.insurance.plan', 'company',
        'Insurance Plans')
    du = fields.Many2One('health.du', 'DU', help="Domiciliary Unit")
    du_address = fields.Function(
        fields.Text('Main address',
        help="Main Address, based on the associated DU"), 'get_du_address')
    birth_certificate = fields.Many2One('health.birth_certificate',
        'Birth Certificate', readonly=True)
    deceased = fields.Boolean('Deceased', readonly=True,
        help='The information is updated from the Death Certificate',
        states={'invisible': Not(Bool(Eval('deceased')))})
    dod = fields.Function(fields.DateTime('Date of Death',
        states={
            'invisible': Not(Bool(Eval('deceased'))),
        },
        depends=['deceased']), 'get_dod')
    death_certificate = fields.Many2One('health.death_certificate',
        'Death Certificate', readonly=True)
    mother = fields.Function(
        fields.Many2One('party.party', 'Mother',
        help="Mother from the Birth Certificate"), 'get_mother')
    father = fields.Function(
        fields.Many2One('party.party', 'Father',
            help="Father from the Birth Certificate"), 'get_father')

    def get_du_address(self, name):
        if (self.du):
            return self.du.address_repr

    def get_mother(self, name):
        if (self.birth_certificate and self.birth_certificate.mother):
            return self.birth_certificate.mother.id

    def get_father(self, name):
        if (self.birth_certificate and self.birth_certificate.father):
            return self.birth_certificate.father.id

    def get_dod(self, name):
        if (self.deceased):
            return self.death_certificate.dod

    @staticmethod
    def default_activation_date():
        return date.today()

    @classmethod
    def generate_puid(cls):
        config = Pool().get('health.configuration').get_config()
        if not config.patient_sequence:
            raise UserError(gettext('msg_sequence_missing'))
        return config.patient_sequence.get()

    @classmethod
    def convert_photo(cls, data):
        if data and Image:
            image = Image.open(BytesIO(data))
            image.thumbnail((200, 200), Image.ANTIALIAS)
            data = BytesIO()
            image.save(data, image.format)
            data = fields.Binary.cast(data.getvalue())
        return data

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        args = []
        for parties, vals in zip(actions, actions):
            given_name = family_name = ''
            vals = vals.copy()
            person_id = parties[0].id
            if vals.get('is_patient') is True:
                vals['puid'] = cls.generate_puid()

            if 'photo' in vals:
                vals['photo'] = cls.convert_photo(vals['photo'])

            if 'name' in vals:
                given_name = family_name = ''
                if 'name' in vals:
                    given_name = vals['name']

                if parties[0].is_person:
                    cls.update_person_official_name(
                        person_id, given_name, family_name
                    )

            args.append(parties)
            args.append(vals)
        return super(Party, cls).write(*args)

    @classmethod
    def update_person_official_name(cls, person_id, given_name, family_name):
        # Create or update the official PersonName entry with the Given / Family
        # names from the main entry field.
        person = []

        Pname = Pool().get('health.person_name')
        officialnames = Pname.search(
            [("party", "=", person_id), ("use", "=", 'official')],
        )

        # If no official name found, create a new record
        if not (officialnames):
            values = {
                'party': person_id,
                'use': 'official',
                }

            if given_name:
                values['given'] = given_name
            if family_name:
                values['family'] = family_name

            person.append(values)
            Pname.create(person)

        #Found a related official name record, then
        #update official Person Name(s) when modified in main form
        else:
            official_rec = []
            official_rec.append(officialnames[0])

            values = {'use': 'official'}

            if given_name:
                values['given'] = given_name
            if family_name:
                values['family'] = family_name

            Pname.write(official_rec, values)

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:

            if values.get('is_patient'):
                values['puid'] = cls.generate_puid()

            values.setdefault('addresses', None)

            if 'photo' in values:
                values['photo'] = cls.convert_photo(values['photo'])

            # If the party is a physical person,
            # add new PersonName record with the given and family name
            # as the official name

            if values.get('is_person'):
                if ('name' in values):
                    official_name = []
                    given_name = family_name = ''

                    if 'name' in values:
                        given_name = values['name']

                    official_name.append(('create', [{
                        'use': 'official',
                        'given': given_name,
                        'family': family_name,
                    }]))

                    values['person_names'] = official_name

        return super(Party, cls).create(vlist)

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('internal_user_uniq', Unique(t, t.internal_user),
                'This internal user is already assigned to a party'),
        ]

        cls._order.insert(1, ('name', 'ASC'))
        # Sort to be used when called from other models.

    def get_rec_name(self, name):
        # Display name on the following sequence
        # 1 - Oficial Name from PersonName with the name representation
        # If not offficial name :
        # 2 - Last name, First name

        if self.person_names:
            prefix = given = family = suffix = ''
            for pname in self.person_names:
                if pname.prefix:
                    prefix = pname.prefix + ' '
                if pname.suffix:
                    suffix = ', ' + pname.suffix

                given = pname.given or ''
                family = pname.family or ''

                res = ''
                if pname.use == 'official':
                    if self.name_representation == 'pgfs':
                        res = prefix + given + ' ' + family + suffix
                    if self.name_representation == 'gf':
                        if pname.family:
                            family = ' ' + pname.family
                        res = given + family
                    if self.name_representation == 'fg':
                        if pname.family:
                            family = pname.family + ', '
                        res = family + given

                    if not self.name_representation:
                        # Default value
                        if family:
                            return family + ', ' + given
                        else:
                            return given
                return res

        return self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        """ Search for the name, PUID, any alternative IDs,
            and any family and / or given name from the person_names
        """
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('puid',) + tuple(clause[1:]),
            ('alternative_ids.code',) + tuple(clause[1:]),
            ('contact_mechanisms.value',) + tuple(clause[1:]),
            ('person_names.family',) + tuple(clause[1:]),
            ('person_names.given',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]

    @fields.depends('is_person', 'is_patient', 'is_healthprof')
    def on_change_with_is_person(self):
        # Set is_person if the party is a health professional or a patient
        if (self.is_healthprof or self.is_patient or self.is_person):
            return True

    @fields.depends('du')
    def on_change_with_du_address(self):
        if (self.du):
            return self.get_du_address(name=None)

    @classmethod
    def validate(cls, parties):
        super(Party, cls).validate(parties)
        for party in parties:
            party.check_person()
            party.validate_official_name()

    def check_person(self):
        # Verify that health professional and patient
        # are unchecked when is_person is False

        if not self.is_person and (self.is_patient or self.is_healthprof):
            raise UserError(gettext(
                "The Person field must be set if the party is a health"
                " professional or a patient"))

    def validate_official_name(self):
        # Only allow one official name on the party name
        Pname = Pool().get('health.person_name')
        officialnames = Pname.search_count(
            [("party", "=", self.id), ("use", "=", 'official')],)

        if (officialnames > 1):
            raise UserError(gettext("The person can have only one official name"))

    @classmethod
    def view_attributes(cls):
        # Hide the group holding all the demographics when the party is not
        # a person
        return [('//group[@id="person_details"]', 'states', {
            'invisible': ~Eval('is_person'),
        })]


class PartyOpenShortcut(Wizard):
    "Open the party creation dialog"
    __name__ = 'party.open'

    @classmethod
    def __setup__(cls):
        super(PartyOpenShortcut, cls).__setup__()


class ContactMechanism(ModelSQL, ModelView):
    __name__ = 'party.contact_mechanism'
    emergency = fields.Boolean('Emergency', select=True)
    remarks = fields.Char('Remarks', help="Enter the name of the contact"
        " or other remarks")


class PersonName(ModelSQL, ModelView):
    "Person Name"
    __name__ = 'health.person_name'

    """ We are using the concept of HumanName on HL7 FHIR
    http://www.hl7.org/implement/standards/fhir/datatypes.html#HumanName
    """

    party = fields.Many2One('party.party', 'Person',
        domain=[('is_person', '=', True)], help="Related party (person)")
    use = fields.Selection([
        (None, ''),
        ('official', 'Official'),
        ('usual', 'Usual'),
        ('nickname', 'Nickname'),
        ('maiden', 'Maiden'),
        ('anonymous', 'Anonymous'),
        ('temp', 'Temp'),
        ('old', 'old'),
        ], 'Use', sort=False, required=True)
    family = fields.Char('Family', help="Family / Surname.")
    given = fields.Char('Given',
        help="Given / First name. May include middle name", required=True)
    prefix = fields.Selection([
        (None, ''),
        ('Mr', 'Mr'),
        ('Mrs', 'Mrs'),
        ('Miss', 'Miss'),
        ('Dr', 'Dr'),
        ], 'Prefix', sort=False)
    suffix = fields.Char('Suffix')
    date_from = fields.Date('From')
    date_to = fields.Date('To')


class PartyAddress(ModelSQL, ModelView):
    'Party Address'
    __name__ = 'party.address'
    relationship = fields.Char('Relationship',
        help='Include the relationship with the person - friend, co-worker,'
        ' brother,...')
    relative_id = fields.Many2One('party.party', 'Contact',
        domain=[('is_person', '=', True)], help='Include link to the relative')
    is_school = fields.Boolean(
        "School", help="Check this box to mark the school address")
    is_work = fields.Boolean(
        "Work", help="Check this box to mark the work address")


class Ethnicity(ModelSQL, ModelView):
    'Ethnicity'
    __name__ = 'health.ethnicity'
    name = fields.Char('Name', required=True, translate=True)
    code = fields.Char('Code', required=True,
        help="Please use CAPITAL LETTERS and no spaces")
    notes = fields.Char('Notes')

    @classmethod
    def __setup__(cls):
        super(Ethnicity, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Name must be unique !'),
            ('code_uniq', Unique(t, t.code), 'The CODE must be unique !'),
        ]


class PartyErase(Wizard):
    "Party Erase"
    __name__ = 'health.addpatient'
    start_state = 'ask'
    ask = StateView('party.party', 'health.health_patient_form', [
        Button("Cancel", 'end', 'tryton-cancel'),
        Button("Erase", 'erase', 'tryton-clear', default=True),
    ])
    erase = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PartyErase, cls).__setup__()


class AlternativePersonID (ModelSQL, ModelView):
    'Alternative person ID'
    __name__ = 'health.person_alternative_identification'

    party = fields.Many2One('party.party', 'Party', readonly=True)
    code = fields.Char('Code', required=True)
    alternative_id_type = fields.Selection([
        (None, ''),
        ('country_id', 'Country of origin ID'),
        ('passport', 'Passport'),
        ('medical_record', 'Medical Record'),
        ('ghealth_federation', 'GNU Health Federation'),
        ('other', 'Other'),
    ], 'ID type', required=True, sort=False,)
    comments = fields.Char('Comments')
