# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import pytz
from datetime import datetime

from trytond.i18n import gettext
from trytond.pool import Pool
from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pyson import Eval, Equal, Or, If, Id
from trytond.exceptions import UserError
from trytond.transaction import Transaction
from .tools import compute_age_from_dates


class PatientEvaluation(ModelSQL, ModelView):
    'Patient Evaluation'
    __name__ = 'health.patient.evaluation'

    STATES = {'readonly': Eval('state') == 'signed'}

    code = fields.Char('Code', states={'readonly': True},
        help="Unique code that identifies the evaluation")
    patient = fields.Many2One('health.patient', 'Patient',
        states={'readonly': Eval('state') != 'in_progress'})
    origin = fields.Reference('Origin', selection='get_origin', select=True,
        states={'readonly': True})
    related_condition = fields.Many2One('health.patient.disease', 'Related condition',
        domain=[('patient', '=', Eval('patient'))], depends=['patient'],
        help="Related condition related to this follow-up evaluation",
        states={'invisible': Eval('visit_type') != 'followup'})
    evaluation_start = fields.DateTime('Start', required=True,
        states=STATES)
    evaluation_endtime = fields.DateTime('End', states=STATES)
    evaluation_length = fields.Function(fields.TimeDelta('Evaluation length',
        help="Duration of the evaluation"), 'evaluation_duration')
    state = fields.Selection([
        (None, ''),
        ('in_progress', 'In progress'),
        ('done', 'Done'),
        ('signed', 'Signed'),
        ], 'State', readonly=True, sort=False)
    next_evaluation = fields.Many2One('health.appointment', 'Next Appointment',
        domain=[('patient', '=', Eval('patient'))], depends=['patient'],
        states=STATES)
    user_id = fields.Many2One('res.user', 'Last Changed by', readonly=True)
    healthprof = fields.Many2One(
        'health.professional', 'Health Prof', readonly=True,
        help="Health professional that initiates the evaluation."
        "This health professional might or might not be the same that"
        " signs and finishes the evaluation."
        "The evaluation remains in progress state until it is signed, "
        "when it becomes read-only")
    signed_by = fields.Many2One(
        'health.professional', 'Health Prof', readonly=True,
        states={'invisible': Equal(Eval('state'), 'in_progress')},
        help="Health Professional that finnished the patient evaluation")
    specialty = fields.Many2One('health.specialty', 'Specialty', states=STATES)
    visit_type = fields.Selection([
        (None, ''),
        ('new', 'New health condition'),
        ('followup', 'Followup'),
        ('well_child', 'Well Child visit'),
        ('well_woman', 'Well Woman visit'),
        ('well_man', 'Well Man visit'),
        ], 'Visit', states=STATES)
    urgency = fields.Selection([
        (None, ''),
        ('a', 'Normal'),
        ('b', 'Urgent'),
        ('c', 'Medical Emergency'),
        ], 'Urgency', sort=False,
        states=STATES)
    computed_age = fields.Function(fields.Char('Age',
        help="Computed patient age at the moment of the evaluation"),
            'patient_age_at_evaluation')
    gender = fields.Function(fields.Selection([
        (None, ''),
        ('m', 'Male'),
        ('f', 'Female'),
        ('f-m', 'Female -> Male'),
        ('m-f', 'Male -> Female'),
        ], 'Gender'), 'get_patient_gender', searcher='search_patient_gender')
    information_source = fields.Char('Source', states=STATES,
        help="Source of Information, eg : Self, relative, friend ...",
    )
    reliable_info = fields.Boolean('Reliable', help="Uncheck this option"
        "if the information provided by the source seems not reliable",
        states=STATES)
    derived_from = fields.Many2One('health.professional', 'Derived from',
        help='Health Professional who derived the case',
        states=STATES)
    derived_to = fields.Many2One('health.professional', 'Derived to',
        help='Health Professional to derive the case',
        states=STATES)
    evaluation_type = fields.Selection([
        (None, ''),
        ('outpatient', 'Outpatient'),
        ('inpatient', 'Inpatient'),
        ], 'Type', sort=False)
    chief_complaint = fields.Char('Chief Complaint', help='Chief Complaint',
        states=STATES)
    notes_complaint = fields.Text('Complaint details',
        states=STATES)
    present_illness = fields.Text('Present Illness',
        states=STATES)
    evaluation_summary = fields.Text('Clinical and physical',
        states=STATES)
    glycemia = fields.Float('Glycemia', states=STATES,
        help='Last blood glucose level. Can be approximative. Expressed in mg/dL or mmol/L.'
    )
    hba1c = fields.Float('Glycated Hemoglobin', states=STATES,
        help='Last Glycated Hb level. Can be approximative. Expressed in mmol/mol.'
    )
    cholesterol_total = fields.Integer('Last Cholesterol', states=STATES,
        help='Last cholesterol reading. Can be approximative. Expressed in mg/dL or mmol/L.',
    )
    hdl = fields.Integer('Last HDL', states=STATES,
        help='Last HDL Cholesterol reading. Can be approximative. Expressed in mg/dL or mmol/L.',
    )
    ldl = fields.Integer('Last LDL', states=STATES,
        help='Last LDL Cholesterol reading. Can be approximative. Expressed in mg/dL or mmol/L.',
    )
    tag = fields.Integer('Last TAGs', states=STATES,
        help='Triacylglycerol(triglicerides) level. Can be approximative. Expressed in mg/dL or mmol/L.',
    )
    systolic = fields.Integer('Systolic Pressure', states=STATES,
        help='Systolic pressure expressed in mmHg')
    diastolic = fields.Integer('Diastolic Pressure', states=STATES,
        help='Diastolic pressure expressed in mmHg')
    bpm = fields.Integer('Heart Rate', states=STATES,
        help='Heart rate expressed in beats per minute',
    )
    respiratory_rate = fields.Integer('Respiratory Rate', states=STATES,
        help='Respiratory rate expressed in breaths per minute',
    )
    osat = fields.Integer('Oxygen Saturation', states=STATES,
        help='Arterial oxygen saturation expressed as a percentage',
    )
    malnutrition = fields.Boolean('Malnutrition', states=STATES,
        help='Check this box if the patient show signs of malnutrition. If'
        ' associated  to a disease, please encode the correspondent disease'
        ' on the patient disease history. For example, Moderate'
        ' protein-energy malnutrition, E44.0 in ICD-10 encoding',
    )
    dehydration = fields.Boolean('Dehydration', states=STATES,
        help='Check this box if the patient show signs of dehydration. If'
        ' associated  to a disease, please encode the  correspondent disease'
        ' on the patient disease history. For example, Volume Depletion, E86'
        ' in ICD-10 encoding')
    temperature = fields.Float('Temperature', help='Temperature in celcius',
        states=STATES)
    weight = fields.Float('Weight', digits=(3, 2), help='Weight in kilos',
        states=STATES)
    height = fields.Float('Height', digits=(3, 1), states=STATES,
        help='Height in centimeters')
    bmi = fields.Float('BMI', digits=(2, 2), help='Body mass index',
        states=STATES)
    fat_percentage = fields.Float('Fat Percentage', digits=(2, 2))
    head_circumference = fields.Float('Head', states=STATES,
        help='Head circumference in centimeters',
    )
    abdominal_circ = fields.Float('Waist', digits=(3, 1), states=STATES,
        help='Waist circumference in centimeters',
    )
    hip = fields.Float('Hip', digits=(3, 1), states=STATES,
        help='Hip circumference in centimeters',
    )
    whr = fields.Float('WHR', digits=(2, 2), states=STATES,
        help='Waist to hip ratio . Reference values:\n'
        'Men : < 0.9 Normal // 0.9 - 0.99 Overweight // > 1 Obesity \n'
        'Women : < 0.8 Normal // 0.8 - 0.84 Overweight // > 0.85 Obesity',
    )
    # DEPRECATION NOTE : SIGNS AND SYMPTOMS FIELDS TO BE REMOVED IN 1.6 .
    # NOW WE USE A O2M OBJECT TO MAKE IT MORE SCALABLE, CLEARER AND FUNCTIONAL
    # TO WORK WITH THE CLINICAL FINDINGS OF THE PATIENT

    diagnosis = fields.Many2One('health.pathology', 'Main Condition',
        help='Presumptive Diagnosis. If no diagnosis can be made, encode the main sign or symptom.',
        states=STATES)
    secondary_conditions = fields.One2Many(
        'health.secondary_condition',
        'evaluation', 'Other Conditions', help='Other '
        ' conditions found on the patient',
        states=STATES)
    diagnostic_hypothesis = fields.One2Many('health.diagnostic_hypothesis',
        'evaluation', 'Hypotheses / DDx', help='Other Diagnostic Hypotheses /'
        ' Differential Diagnosis (DDx)',
        states=STATES)
    signs_and_symptoms = fields.One2Many('health.signs_and_symptoms',
        'evaluation', 'Signs and Symptoms', help='Enter the Signs and Symptoms'
        ' for the patient in this evaluation.',
        states=STATES)
    info_diagnosis = fields.Text('Presumptive Diagnosis: Extra Info',
        states=STATES)
    directions = fields.Text('Plan', states=STATES)
    actions = fields.One2Many('health.directions', 'evaluation', 'Procedures',
        help='Procedures / Actions to take',  states=STATES)
    notes = fields.Text('Notes', states=STATES)
    discharge_reason = fields.Selection([
            (None, ''),
            ('home', 'Home / Selfcare'),
            ('transfer', 'Transferred to another institution'),
            ('against_advice', 'Left against medical advice'),
            ('death', 'Death')
        ], 'Discharge Reason', required=True, sort=False,
        states={
            'invisible': Equal(Eval('state'), 'in_progress'),
            'readonly': Eval('state') == 'signed'
        },
        help="Reason for patient discharge")
    institution = fields.Many2One('health.institution', 'Institution',
        states={'readonly': True}, required=True)
    report_evaluation_date = fields.Function(fields.Date(
        'Evaluation Date'), 'get_report_evaluation_date')
    report_evaluation_time = fields.Function(fields.Time(
        'Evaluation Time'), 'get_report_evaluation_time')
    # obstetrics = fields.One2Many('health.evaluation.obstetrics',
    #     'evaluation', 'Obstetrics', states={
    #         'invisible': Eval('specialty') != 'obstetrics'
    #     })
    # nutrition = fields.One2Many('health.evaluation.nutrition',
    #     'evaluation', 'Nutrition', states={
    #         'invisible': Eval('specialty') != 'nutrition'
    # })
    product = fields.Many2One('product.product', 'Product',
        help='Evaluation product to invoice.')
    prescriptions = fields.One2Many('health.prescription.order',
        'origin', 'Prescriptions', states={
            'readonly': Eval('state').in_(['signed', 'done'])
        }, domain=[
            ('patient', '=', Eval('patient')),
        ])
    mental_evaluations = fields.One2Many('health.evaluation.mental', 'evaluation',
        'Mental Evaluations', states={
            'invisible': If(
                Eval('specialty', None),
                ~Eval('specialty', 0).in_([
                    Id('health', 'psychiatry'),
                    Id('health', 'psychology')
                ]), True
            ),
            'readonly': Eval('state') == 'signed'
        }, depends=['specialty'],
            domain=[('patient', '=', Eval('patient'))]
        )

    @classmethod
    def __setup__(cls):
        super(PatientEvaluation, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [(
            'code_unique', Unique(t, t.code),
            'The evaluation code must be unique !'
        )]
        cls._buttons.update({
            'end_evaluation': {
                'invisible': Or(Equal(Eval('state'), 'signed'), Equal(Eval('state'), 'done'))
            },
        })

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.appointment']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @classmethod
    def _get_specialty_record(cls):
        'Return list of Model names for specialty'
        return []

    @classmethod
    def get_specialty_record(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_specialty_record()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_discharge_reason():
        return 'home'

    def get_patient_gender(self, name):
        return self.patient.gender

    @classmethod
    def search_patient_gender(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('patient.party.gender', clause[1], value))
        return res

    @classmethod
    def validate(cls, evaluations):
        super(PatientEvaluation, cls).validate(evaluations)
        for evaluation in evaluations:
            evaluation.validate_evaluation_period()
            evaluation.check_health_professional()

    def patient_age_at_evaluation(self, name):
        if (self.patient.party.dob and self.evaluation_start):
            return compute_age_from_dates(self.patient.party.dob, None,
                        None, None, 'age', self.evaluation_start.date())

    def evaluation_duration(self, name):
        if (self.evaluation_endtime and self.evaluation_start):
            return self.evaluation_endtime - self.evaluation_start

    def validate_evaluation_period(self):
        Lang = Pool().get('ir.lang')
        language, = Lang.search([('code', '=', Transaction().language)])
        if self.evaluation_endtime and self.evaluation_start:
            if (self.evaluation_endtime < self.evaluation_start):
                raise UserError(gettext('health.msg_end_date_before_start', {
                    'evaluation_start': Lang.strftime(
                        self.evaluation_start, language.code, language.date),
                    'evaluation_endtime': Lang.strftime(
                        self.evaluation_endtime, language.code, language.date),
                }))

    def check_health_professional(self):
        if not self.healthprof:
            raise UserWarning(gettext('health.msg_health_professional_warning'))

    @staticmethod
    def default_healthprof():
        HealthProfessional = Pool().get('health.professional')
        return HealthProfessional.get_health_professional()

    @staticmethod
    def default_loc_eyes():
        return '4'

    @staticmethod
    def default_loc_verbal():
        return '5'

    @staticmethod
    def default_loc_motor():
        return '6'

    @staticmethod
    def default_loc():
        return 15

    @staticmethod
    def default_evaluation_type():
        return 'outpatient'

    @staticmethod
    def default_state():
        return 'in_progress'

    @fields.depends('weight', 'height')
    def on_change_with_bmi(self):
        if self.height and self.weight:
            if (self.height > 0):
                return round(self.weight / ((self.height / 100) ** 2), 2)
            return 0

    @fields.depends('weight', 'height', 'bmi')
    def on_change_bmi(self):
        if self.height and self.weight:
            if (self.height > 0):
                self.bmi = round(self.weight / ((self.height / 100) ** 2), 2)
        elif (self.height and not self.weight):
            self.weight = round((((self.height / 100) ** 2) * self.bmi), 2)

        elif (self.weight and not self.height):
            self.height = round(((self.weight / self.bmi)**(0.5)*100), 2)

    @fields.depends('loc_verbal', 'loc_motor', 'loc_eyes')
    def on_change_with_loc(self):
        return int(self.loc_motor) + int(self.loc_eyes) + int(self.loc_verbal)

    @fields.depends('patient')
    def on_change_patient(self):
        self.gender = self.patient.gender
        self.computed_age = self.patient.age

    @staticmethod
    def default_reliable_info():
        return True

    @staticmethod
    def default_evaluation_start():
        return datetime.now()

    # Calculate the WH ratio
    @fields.depends('abdominal_circ', 'hip', 'whr')
    def on_change_with_whr(self):
        waist = self.abdominal_circ
        hip = self.hip
        if (hip > 0):
            whr = round((waist / hip), 2)
        else:
            whr = 0
        return whr

    def get_rec_name(self, name):
        return str(self.evaluation_start)

    def get_report_evaluation_date(self, name):
        Company = Pool().get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.evaluation_start
        return datetime.astimezone(dt.replace(tzinfo=pytz.utc), timezone).date()

    def get_report_evaluation_time(self, name):
        Company = Pool().get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.evaluation_start
        return datetime.astimezone(dt.replace(tzinfo=pytz.utc), timezone).time()

    @classmethod
    def create(cls, vlist):
        config = Pool().get('health.configuration').get_config()

        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                values['code'] = config.patient_evaluation_sequence.get()

        return super(PatientEvaluation, cls).create(vlist)

    @classmethod
    @ModelView.button_action('health.open_patient_history')
    def see_history(cls, evaluations):
        pass

    @classmethod
    @ModelView.button
    def end_evaluation(cls, evaluations):
        pool = Pool()
        HealthProfessional = pool.get('health.professional')

        # Change the state of the evaluation to "Done"

        signing_hp = HealthProfessional.get_health_professional()
        cls.write(evaluations, {
            'state': 'done',
            'signed_by': signing_hp,
            'evaluation_endtime': datetime.now()
        })

        # If there is an appointment associated to this evaluation
        # set it to state "Done"
        if evaluations:
            origin = evaluations[0].origin
            if origin.__name__ == 'health.appointment':
                Appointment = pool.get('health.appointment')
                Appointment.write([origin], {'state': 'done'})


class Obstetrics(ModelSQL, ModelView):
    'Evaluation Obstetrics'
    __name__ = 'health.evaluation.obstetrics'
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation')
    date_last_menstruation = fields.Date('FUM')
    total_births = fields.Numeric('Total Births')


class Nutrition(ModelSQL, ModelView):
    'Evaluation Nutrition'
    __name__ = 'health.evaluation.nutrition'
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation')
    nutritional_status = fields.Char('Nutritional Status')


class Directions(ModelSQL, ModelView):
    'Patient Directions'
    __name__ = 'health.directions'
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        readonly=True)
    procedure = fields.Many2One('health.procedure', 'Procedure', required=True)
    comments = fields.Char('Comments')


# SECONDARY CONDITIONS ASSOCIATED TO THE PATIENT IN THE EVALUATION
class SecondaryCondition(ModelSQL, ModelView):
    'Secondary Conditions'
    __name__ = 'health.secondary_condition'
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        readonly=True)
    pathology = fields.Many2One('health.pathology', 'Pathology', required=True)
    comments = fields.Char('Comments')


class DiagnosticHypothesis(ModelSQL, ModelView):
    'Other Diagnostic Hypothesis'
    __name__ = 'health.diagnostic_hypothesis'
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        readonly=True)
    pathology = fields.Many2One('health.pathology', 'Pathology', required=True)
    comments = fields.Char('Comments')


class SignsAndSymptoms(ModelSQL, ModelView):
    'Evaluation Signs and Symptoms'
    __name__ = 'health.signs_and_symptoms'

    evaluation = fields.Many2One(
        'health.patient.evaluation', 'Evaluation', readonly=True)
    sign_or_symptom = fields.Selection([
        (None, ''),
        ('sign', 'Sign'),
        ('symptom', 'Symptom')],
        'Subjective / Objective', required=True)
    clinical = fields.Many2One(
        'health.pathology', 'Sign or Symptom',
        required=True)
    comments = fields.Char('Comments')


class EvaluationMental(ModelSQL, ModelView):
    'Patient Evaluation Mental'
    __name__ = 'health.evaluation.mental'

    evaluation = fields.Many2One('health.patient.evaluation',
        'Evaluation', readonly=True)
    patient = fields.Many2One('health.patient', 'Patient', states={
        'readonly': True
    })
    loc = fields.Integer('Glasgow',
        help='Level of Consciousness - on Glasgow Coma Scale :  < 9 severe -'
        ' 9-12 Moderate, > 13 minor',)
    loc_eyes = fields.Selection([
        ('1', 'Does not Open Eyes'),
        ('2', 'Opens eyes in response to painful stimuli'),
        ('3', 'Opens eyes in response to voice'),
        ('4', 'Opens eyes spontaneously'),
        ], 'Glasgow - Eyes', sort=True)
    loc_verbal = fields.Selection([
        ('1', 'Makes no sounds'),
        ('2', 'Incomprehensible sounds'),
        ('3', 'Utters inappropriate words'),
        ('4', 'Confused, disoriented'),
        ('5', 'Oriented, converses normally'),
        ], 'Glasgow - Verbal', sort=True)
    loc_motor = fields.Selection([
        ('1', 'Makes no movement'),
        ('2', 'Extension to painful stimuli - decerebrate response -'),
        ('3', 'Abnormal flexion to painful stimuli (decorticate response)'),
        ('4', 'Flexion / Withdrawal to painful stimuli'),
        ('5', 'Localizes painful stimuli'),
        ('6', 'Obeys commands'),
        ], 'Glasgow - Motor', sort=True)
    tremor = fields.Boolean('Tremor',
        help='If associated  to a disease, please encode it on the patient'
        ' disease history')
    violent = fields.Boolean('Violent Behaviour',
        help='Check this box if the patient is aggressive or violent at the'
        ' moment')
    mood = fields.Selection([
        (None, ''),
        ('n', 'Normal'),
        ('s', 'Sad'),
        ('f', 'Fear'),
        ('r', 'Rage'),
        ('h', 'Happy'),
        ('d', 'Disgust'),
        ('e', 'Euphoria'),
        ('fl', 'Flat'),
        ('an', 'anxious'),
        ], 'Mood', sort=True)
    orientation = fields.Boolean('Orientation',
        help='Check this box if the patient is disoriented in time and/or space')
    memory = fields.Boolean('Memory',
        help='Check this box if the patient has problems in short or long term memory',
    )
    knowledge_current_events = fields.Boolean('Knowledge of Current Events',
        help='Check this box if the patient can not respond to public'
        ' notorious events',
    )
    judgment = fields.Boolean('Judgment',
        help='Check this box if the patient can not interpret basic scenario'
        ' solutions',
    )
    abstraction = fields.Boolean('Abstraction',
        help='Check this box if the patient presents abnormalities in'
        ' abstract reasoning')
    vocabulary = fields.Boolean('Vocabulary',
        help='Check this box if the patient lacks basic intelectual capacity,'
        ' when she/he can not describe elementary objects',
    )
    calculation_ability = fields.Boolean('Calculation Ability',
        help='Check this box if the patient can not do simple arithmetic problems',
    )
    object_recognition = fields.Boolean('Object Recognition',
        help='Check this box if the patient suffers from any sort of gnosia'
        ' disorders, such as agnosia, prosopagnosia ...')
    praxis = fields.Boolean('Praxis',
        help='Check this box if the patient is unable to make voluntary'
        'movements')
