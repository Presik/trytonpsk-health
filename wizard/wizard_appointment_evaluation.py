
from trytond.i18n import gettext
from trytond.wizard import Wizard, StateAction
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder
from trytond.exceptions import UserError


class CreateAppointmentEvaluation(Wizard):
    'Create Appointment Evaluation'
    __name__ = 'wizard.health.appointment.evaluation'

    start_state = 'appointment_evaluation'
    appointment_evaluation = StateAction('health.act_app_evaluation')

    def do_appointment_evaluation(self, action):
        appointment_id = Transaction().context.get('active_id')
        Appointment = Pool().get('health.appointment')
        try:
            appointment = Appointment.browse([appointment_id])[0]
        except:
            raise UserError(gettext('health.msg_no_record_selected'))

        patient = appointment.patient.id

        if (appointment.speciality):
            specialty = appointment.speciality.id
        else:
            specialty = None
        urgency = str(appointment.urgency)
        evaluation_type = str(appointment.appointment_type)
        visit_type = str(appointment.visit_type)

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient', '=', patient),
            ('specialty', '=', specialty),
            ('urgency', '=', urgency),
            ('evaluation_type', '=', evaluation_type),
            ('visit_type', '=', visit_type),
            ('origin', '=', str(appointment)),
        ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': patient,
            'specialty': specialty,
            'urgency': urgency,
            'evaluation_type': evaluation_type,
            'visit_type': visit_type,
            'origin': str(appointment),
        })

        return action, {}
