# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval


class Product(ModelSQL, ModelView):
    __name__ = 'product.product'
    kind = fields.Selection([
        (None, ''),
        ('medicament', 'Medicament'),
        ('medical_supply', 'Medical Supply'),
        ('vaccine', 'Vaccine'),
        ('bed', 'Bed'),
        ('insurance_plan', 'Insurance Plan'),
        ('procedure', 'Procedure'),
        ], 'Kind')
    procedure = fields.Many2One('health.procedure', 'Procedure', states={
        'invisible': Eval('kind') != 'procedure',
    })

    @classmethod
    def check_xml_record(cls, records, values):
        return True


class ProductTemplate(ModelSQL, ModelView):
    __name__ = 'product.template'
    """
    Allow to change the values from the product templates
    coming from XML files
    """
    @classmethod
    def check_xml_record(cls, records, values):
        return True
