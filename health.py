
import pytz
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from urllib.parse import urlencode
from urllib.parse import urlunparse
from collections import OrderedDict
try:
    from PIL import Image
except ImportError:
    Image = None
from sql import Literal, Join

from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.wizard import Wizard, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pyson import Eval, Not, Bool, PYSONEncoder, Equal, Or
from trytond.pool import Pool
from trytond.i18n import gettext
from trytond.exceptions import UserError
from .tools import compute_age_from_dates


YES_NO = [
    (None, ''),
    ('yes', 'Yes'),
    ('no', 'No'),
    ]


class DomiciliaryUnit(ModelSQL, ModelView):
    'Domiciliary Unit'
    __name__ = 'health.du'
    name = fields.Char('Code', required=True)
    desc = fields.Char('Desc')
    address_street = fields.Char('Street')
    address_street_number = fields.Integer('Number')
    address_street_bis = fields.Char('Apartment')
    address_district = fields.Char(
        'District', help="Neighborhood, Village, Barrio....")
    address_municipality = fields.Char(
        'Municipality', help="Municipality, Township, county ..")
    address_city = fields.Char('City')
    address_zip = fields.Char('Zip Code')
    address_country = fields.Many2One(
        'country.country', 'Country', help='Country')
    address_subdivision = fields.Many2One(
        'country.subdivision', 'Subdivision',
        domain=[('country', '=', Eval('address_country'))],
        depends=['address_country'])
    operational_sector = fields.Many2One('health.operational_sector',
        'Operational Sector')
    picture = fields.Binary('Picture')
    latitude = fields.Numeric('Latitude', digits=(3, 14))
    longitude = fields.Numeric('Longitude', digits=(4, 14))
    altitude = fields.Integer('Altitude', help="Altitude in meters")
    urladdr = fields.Char('OSM Map',
        help="Locates the DU on the Open Street Map by default")

    # Text Representation
    address_repr = fields.Function(fields.Text("DU Address"), 'get_du_address')
    dwelling = fields.Selection([
        (None, ''),
        ('single_house', 'Single / Detached House'),
        ('apartment', 'Apartment'),
        ('townhouse', 'Townhouse'),
        ('factory', 'Factory'),
        ('building', 'Building'),
        ('mobilehome', 'Mobile House'),
        ], 'Type', sort=False)
    materials = fields.Selection([
        (None, ''),
        ('concrete', 'Concrete'),
        ('adobe', 'Adobe'),
        ('wood', 'Wood'),
        ('mud', 'Mud / Straw'),
        ('stone', 'Stone'),
        ], 'Material', sort=False)
    roof_type = fields.Selection([
        (None, ''),
        ('concrete', 'Concrete'),
        ('adobe', 'Adobe'),
        ('wood', 'Wood'),
        ('mud', 'Mud'),
        ('thatch', 'Thatched'),
        ('stone', 'Stone'),
        ], 'Roof', sort=False)
    total_surface = fields.Integer('Surface', help="Surface in sq. meters")
    bedrooms = fields.Integer('Bedrooms')
    bathrooms = fields.Integer('Bathrooms')
    housing = fields.Selection([
        (None, ''),
        ('0', 'Shanty, deficient sanitary conditions'),
        ('1', 'Small, crowded but with good sanitary conditions'),
        ('2', 'Comfortable and good sanitary conditions'),
        ('3', 'Roomy and excellent sanitary conditions'),
        ('4', 'Luxury and excellent sanitary conditions'),
        ], 'Conditions',
        help="Housing and sanitary living conditions", sort=False)
    sewers = fields.Boolean('Sanitary Sewers')
    water = fields.Boolean('Running Water')
    trash = fields.Boolean('Trash recollection')
    electricity = fields.Boolean('Electrical supply')
    gas = fields.Boolean('Gas supply')
    telephone = fields.Boolean('Telephone')
    television = fields.Boolean('Television')
    internet = fields.Boolean('Internet')
    members = fields.One2Many('party.party', 'du', 'Members', readonly=True)

    def get_parent(self, subdivision):
        # Recursively get the parent subdivisions
        if (subdivision.parent):
            return str(subdivision.rec_name) + '\n' + \
                str(self.get_parent(subdivision.parent))
        else:
            return subdivision.rec_name

    def get_du_address(self, name):
        du_addr = ''
        # Street
        if (self.address_street):
            du_addr = str(self.address_street) + ' ' + \
                str(self.address_street_number) + "\n"

        # Grab the parent subdivisions
        if (self.address_subdivision):
            du_addr = du_addr + \
                str(self.get_parent(subdivision=self.address_subdivision))

        # Zip Code
        if (self.address_zip):
            du_addr = du_addr + " - " + self.address_zip

        # Country
        if (self.address_country):
            du_addr = du_addr + "\n" + self.address_country.rec_name

        return du_addr

    @fields.depends('latitude', 'longitude', 'address_street', 'address_city',
        'address_street_number', 'address_district', 'address_municipality',
        'address_zip', 'address_subdivision', 'address_country')
    def on_change_with_urladdr(self):
        # Generates the URL to be used in OpenStreetMap
        #   If latitude and longitude are known, use them.
        #   Otherwise, use street, municipality, city, and so on.

        parts = OrderedDict()
        parts['scheme'] = 'http'
        parts['netloc'] = ''
        parts['path'] = ''
        parts['params'] = ''
        parts['query'] = ''
        parts['fragment'] = ''

        if (self.latitude and self.longitude):
            parts['netloc'] = 'openstreetmap.org'
            parts['path'] = '/'
            parts['query'] = urlencode({'mlat': self.latitude,
                                        'mlon': self.longitude})
        else:
            state = country = postalcode = city = municipality = street = number = ''
            if self.address_street_number is not None:
                number = str(self.address_street_number)
            if self.address_street:
                street = self.address_street
            if self.address_municipality:
                municipality = self.address_municipality
            if self.address_city:
                city = self.address_city
            if self.address_zip:
                postalcode = self.address_zip
            if self.address_subdivision:
                state = self.address_subdivision.name
            if self.address_country:
                country = self.address_country.code

            parts['netloc'] = 'nominatim.openstreetmap.org'
            parts['path'] = 'search'
            parts['query'] = urlencode({
                'street': ' '.join([number, street]).strip(),
                'county': municipality,
                'city': city,
                'state': state,
                'postalcode': postalcode,
                'country': country
            })
        return urlunparse(list(parts.values()))

    # Show the resulting Address representation in realtime
    @fields.depends('address_street', 'address_subdivision',
        'address_street_number', 'address_country')
    def on_change_with_address_repr(self):
        return self.get_du_address(name=None)

    @classmethod
    def __setup__(cls):
        super(DomiciliaryUnit, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_uniq', Unique(t, t.name),
             'The Domiciliary Unit must be unique !')
        ]


class DrugDoseUnits(ModelSQL, ModelView):
    'Drug Dose Unit'
    __name__ = 'health.dose.unit'
    name = fields.Char('Unit', required=True, select=True, translate=True)
    desc = fields.Char('Description', translate=True)

    @classmethod
    def __setup__(cls):
        super(DrugDoseUnits, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Unit must be unique !'),
        ]


class MedicationFrequency(ModelSQL, ModelView):
    'Medication Common Frequencies'
    __name__ = 'health.medication.dosage'

    name = fields.Char('Frequency', required=True, select=True, translate=True,
        help='Common frequency name')
    code = fields.Char('Code', required=True,
        help='Dosage Code,for example: SNOMED 229798009 = 3 times per day.'
           'Please use CAPITAL LETTERS and no spaces')
    abbreviation = fields.Char('Abbreviation',
        help='Dosage abbreviation, such as tid in the US or tds in the UK')

    @classmethod
    def __setup__(cls):
        super(MedicationFrequency, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Unit must be unique !'),
            ('code_uniq', Unique(t, t.code), 'The CODE must be unique !'),
        ]


class DrugForm(ModelSQL, ModelView):
    'Drug Form'
    __name__ = 'health.drug.form'

    name = fields.Char('Form', required=True, select=True, translate=True)
    code = fields.Char('Code', required=True,
        help="Please use CAPITAL LETTERS and no spaces")

    @classmethod
    def __setup__(cls):
        super(DrugForm, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Unit must be unique !'),
            ('code_uniq', Unique(t, t.code), 'The CODE must be unique !'),
        ]


class DrugRoute(ModelSQL, ModelView):
    'Drug Administration Route'
    __name__ = 'health.drug.route'

    name = fields.Char('Unit', required=True, select=True, translate=True)
    code = fields.Char('Code', required=True,
        help="Please use CAPITAL LETTERS and no spaces")

    @classmethod
    def __setup__(cls):
        super(DrugRoute, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Name must be unique !'),
            ('code_uniq', Unique(t, t.code), 'The CODE must be unique !'),
        ]


class Occupation(ModelSQL, ModelView):
    'Occupation'
    __name__ = 'health.occupation'

    name = fields.Char('Name', required=True, translate=True)
    code = fields.Char('Code',  required=True,
        help="Please use CAPITAL LETTERS and no spaces")

    @classmethod
    def __setup__(cls):
        super(Occupation, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Name must be unique !'),
            ('code_uniq', Unique(t, t.code), 'The CODE must be unique !'),
        ]


class OperationalArea(ModelSQL, ModelView):
    'Operational Area'
    __name__ = 'health.operational_area'
    name = fields.Char('Name', required=True,
        help='Operational Area of the city or region')
    operational_sector = fields.One2Many('health.operational_sector',
        'operational_area', 'Operational Sector', readonly=True)
    info = fields.Text('Extra Information')

    @classmethod
    def __setup__(cls):
        super(OperationalArea, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('name_uniq', Unique(t, t.name),
                'The operational area must be unique !'),
        ]


class OperationalSector(ModelSQL, ModelView):
    'Operational Sector'
    __name__ = 'health.operational_sector'
    name = fields.Char('Op. Sector', required=True,
        help='Region included in an operational area')
    operational_area = fields.Many2One(
        'health.operational_area', 'Operational Area')
    info = fields.Text('Extra Information')

    @classmethod
    def __setup__(cls):
        super(OperationalSector, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('name_uniq', Unique(t, t.name, t.operational_area),
                'The operational sector must be unique in each'
                ' operational area!'),
        ]


class MedicalSpecialty(ModelSQL, ModelView):
    'Medical Specialty'
    __name__ = 'health.specialty'
    name = fields.Char('Specialty', required=True, translate=True,
        help='ie, Addiction Psychiatry')
    code = fields.Char('Code', required=True,
        help='ie, ADP. Please use CAPITAL LETTERS and no spaces')

    @classmethod
    def __setup__(cls):
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Specialty must be unique !'),
            ('code_uniq', Unique(t, t.code), 'The CODE must be unique !'),
        ]
        super(MedicalSpecialty, cls).__setup__()


class HealthProfessional(ModelSQL, ModelView):
    'Health Professional'
    __name__ = 'health.professional'
    party = fields.Many2One('party.party', 'Health Professional',
        required=True,
        help='Health Professional\'s Name, from the partner list',
        domain=[
            ('is_healthprof', '=', True),
            ('is_person', '=', True),
        ])
    users = fields.One2Many('res.user', 'healthprof', 'Users', add_remove=[])
    institution = fields.Many2One(
        'health.institution', 'Institution',
        help='Main institution where she/he works')
    code = fields.Char('LICENSE ID', help='License ID')
    specialties = fields.One2Many('health.hp_specialty', 'healthprof',
        'Specialties')
    info = fields.Text('Extra info')
    puid = fields.Function(fields.Char('PUID',
        help="Person Unique Identifier"), 'get_hp_puid',
        searcher='search_hp_puid')
    active = fields.Boolean('Active', select=True)

    @classmethod
    def get_health_professional(cls):
        # Get the professional associated to the internal user id
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if user.healthprof:
            return user.healthprof.id
        else:
            raise UserError(gettext("health.msg_health_professional_warning"))

    @staticmethod
    def default_active():
        return True

    def get_hp_puid(self, name):
        return self.party.puid

    @classmethod
    def search_hp_puid(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('party.puid', clause[1], value))
        return res

    @classmethod
    def __setup__(cls):
        super(HealthProfessional, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('hp_uniq', Unique(t, t.party),
                'The health professional must be unique'),
            ('code_uniq', Unique(t, t.code),
                'The LICENSE ID must be unique'),
        ]

    def get_rec_name(self, name):
        if self.party:
            return self.party.name
        #     if self.party.lastname:
        #         res = self.party.lastname + ', ' + self.party.name
        # return res


# class HealthProfessionalUser(ModelSQL):
#     'Health Professional - User'
#     __name__ = 'health.professional-res.user'
#     _table = 'health_professional_res_user'
#     healthprof = fields.Many2One('health.professional',
#         'Health Professional', ondelete='CASCADE', select=True, required=True)
#     user = fields.Many2One('res.user', 'User', ondelete='RESTRICT',
#         required=True)


class HealthProfessionalSpecialties(ModelSQL, ModelView):
    'Health Professional Specialties'
    __name__ = 'health.hp_specialty'

    healthprof = fields.Many2One('health.professional', 'Health Professional')
    specialty = fields.Many2One(
        'health.specialty', 'Specialty', help='Specialty Code')

    def get_rec_name(self, name):
        return self.specialty.name


class PhysicianSP(ModelSQL, ModelView):
    # Add Main Specialty field after from the Health Professional Speciality
    'Health Professional'
    __name__ = 'health.professional'

    main_specialty = fields.Many2One('health.hp_specialty', 'Main Specialty',
        domain=[('specialty', '=', Eval('id'))],
        states={'readonly': Eval('id', 0) < 0},
        depends=['id'])


class Family(ModelSQL, ModelView):
    'Family'
    __name__ = 'health.family'
    name = fields.Char('Family', required=True, help='Family code')
    members = fields.One2Many('health.family_member', 'family',
        'Family Members')
    info = fields.Text('Extra Information')

    @classmethod
    def __setup__(cls):
        super(Family, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name), 'The Family Code must be unique !'),
        ]


class FamilyMember(ModelSQL, ModelView):
    'Family Member'
    __name__ = 'health.family_member'

    family = fields.Many2One('health.family', 'Family', required=True,
        readonly=True, help='Family code')
    party = fields.Many2One('party.party', 'Party', required=True,
        domain=[('is_person', '=', True)],
        help='Family Member')
    role = fields.Char('Role', help='Father, Mother, sibbling...')


class MedicamentCategory(ModelSQL, ModelView):
    'Medicament Category'
    __name__ = 'health.medicament.category'
    name = fields.Char('Name', required=True, translate=True)
    parent = fields.Many2One(
        'health.medicament.category', 'Parent', select=True)
    childs = fields.One2Many(
        'health.medicament.category', 'parent', string='Children')

    @classmethod
    def __setup__(cls):
        super(MedicamentCategory, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @classmethod
    def validate(cls, categories):
        super(MedicamentCategory, cls).validate(categories)

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + ' / ' + self.name
        else:
            return self.name


class Medicament(ModelSQL, ModelView):
    'Medicament'
    __name__ = 'health.medicament'

    description = fields.Char('Description', required=True)
    product = fields.Many2One('product.product', 'Product', required=False,
        domain=[('kind', '=', 'medicament')], help='Product Name')
    active_component = fields.Char('Active component', translate=True,
        help='Active Component')
    category = fields.Many2One('health.medicament.category', 'Category')
    therapeutic_action = fields.Char('Therapeutic effect',
        help='Therapeutic action')
    composition = fields.Text('Composition', help='Components')
    indications = fields.Text('Indication', help='Indications')
    strength = fields.Float('Strength',
        help='Amount of medication (eg, 250 mg) per dose')
    unit = fields.Many2One('health.dose.unit', 'dose unit',
        help='Unit of measure for the medication to be taken')
    route = fields.Many2One('health.drug.route', 'Administration Route',
        help='Drug administration route code.')
    form = fields.Many2One('health.drug.form', 'Form',
        help='Drug form, such as tablet, suspension, liquid ..')
    sol_conc = fields.Float('Concentration',
        help='Solution drug concentration')
    sol_conc_unit = fields.Many2One(
        'health.dose.unit', 'Unit',
        help='Unit of the drug concentration')
    sol_vol = fields.Float(
        'Volume',
        help='Solution concentration volume')

    sol_vol_unit = fields.Many2One('health.dose.unit', 'Unit',
        help='Unit of the solution volume')
    dosage = fields.Text('Dosage Instructions', help='Dosage / Indications')
    overdosage = fields.Text('Overdosage', help='Overdosage')
    pregnancy_warning = fields.Boolean('Pregnancy Warning',
        help='The drug represents risk to pregnancy or lactancy')
    pregnancy = fields.Text(
        'Pregnancy and Lactancy', help='Warnings for Pregnant Women')
    pregnancy_category = fields.Selection([
        (None, ''),
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('X', 'X'),
        ('N', 'N'),
        ], 'Pregnancy Category',
        help='** FDA Pregnancy Categories ***\n'
        'CATEGORY A :Adequate and well-controlled human studies have failed'
        ' to demonstrate a risk to the fetus in the first trimester of'
        ' pregnancy (and there is no evidence of risk in later'
        ' trimesters).\n\n'
        'CATEGORY B : Animal reproduction studies have failed todemonstrate a'
        ' risk to the fetus and there are no adequate and well-controlled'
        ' studies in pregnant women OR Animal studies have shown an adverse'
        ' effect, but adequate and well-controlled studies in pregnant women'
        ' have failed to demonstrate a risk to the fetus in any'
        ' trimester.\n\n'
        'CATEGORY C : Animal reproduction studies have shown an adverse'
        ' effect on the fetus and there are no adequate and well-controlled'
        ' studies in humans, but potential benefits may warrant use of the'
        ' drug in pregnant women despite potential risks. \n\n '
        'CATEGORY D : There is positive evidence of human fetal  risk based'
        ' on adverse reaction data from investigational or marketing'
        ' experience or studies in humans, but potential benefits may warrant'
        ' use of the drug in pregnant women despite potential risks.\n\n'
        'CATEGORY X : Studies in animals or humans have demonstrated fetal'
        ' abnormalities and/or there is positive evidence of human fetal risk'
        ' based on adverse reaction data from investigational or marketing'
        ' experience, and the risks involved in use of the drug in pregnant'
        ' women clearly outweigh potential benefits.\n\n'
        'CATEGORY N : Not yet classified')

    presentation = fields.Text('Presentation', help='Packaging')
    adverse_reaction = fields.Text('Adverse Reactions')
    storage = fields.Text('Storage Conditions')
    is_vaccine = fields.Boolean('Vaccine')
    notes = fields.Text('Extra Info')
    active = fields.Boolean('Active', select=True)
    # Show the icon depending on the pregnancy category
    pregnancy_cat_icon = fields.Function(fields.Char('Preg. Cat. Icon'),
        'get_preg_cat_icon')

    @staticmethod
    def default_active():
        return True

    def get_preg_cat_icon(self, name):
        if (self.pregnancy_category == 'X'):
            return 'health-stop'
        if (self.pregnancy_category == 'D' or self.pregnancy_category == "C"):
            return 'health-warning'

    def get_rec_name(self, name):
        return self.description or self.product and self.product.name

    # Allow to search by name, active component or category
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('product',) + tuple(clause[1:]),
            ('active_component',) + tuple(clause[1:]),
            ('category',) + tuple(clause[1:]),
        ]

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    @fields.depends('name', 'description')
    def on_change_name(self):
        if self.product:
            self.description = self.product.name


class ImmunizationScheduleDose(ModelSQL, ModelView):
    'Immunization Schedule Dose'
    __name__ = 'health.immunization_schedule_dose'

    vaccine = fields.Many2One(
        'health.immunization_schedule_line', 'Vaccine', required=True,
        help='Vaccine Name')
    dose_number = fields.Integer('Dose', required=True)
    age_dose = fields.Integer('Age', required=True)
    age_unit = fields.Selection([
        (None, ''),
        ('days', 'days'),
        ('weeks', 'weeks'),
        ('months', 'months'),
        ('years', 'years'),
        ], 'Time Unit', required=True)

    remarks = fields.Char('Remarks')

    sched = fields.Function(
        fields.Char('Schedule'), 'get_dose_schedule',
        searcher='search_dose_schedule')

    def get_dose_schedule(self, name):
        return self.vaccine.sched.sched

    @classmethod
    def search_dose_schedule(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('vaccine.sched.sched', clause[1], value))
        return res

    @classmethod
    def __setup__(cls):
        super(ImmunizationScheduleDose, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('number_uniq', Unique(t, t.dose_number, t.vaccine),
                'The dose number must be unique for this vaccine'),
        ]
        cls._order.insert(0, ('vaccine', 'ASC'))
        cls._order.insert(1, ('dose_number', 'ASC'))


class ImmunizationScheduleLine(ModelSQL, ModelView):
    'Immunization Schedule Line'
    __name__ = 'health.immunization_schedule_line'

    sched = fields.Many2One('health.immunization_schedule', 'Schedule',
        required=True, help='Schedule Name')
    vaccine = fields.Many2One('health.medicament', 'Vaccine', required=True,
        domain=[('product.kind', '=', 'vaccine')], help='Vaccine Name')
    scope = fields.Selection([
        (None, ''),
        ('systematic', 'Systematic'),
        ('recommended', 'Recommended'),
        ('highrisk', 'Risk groups'),
        ], 'Scope', sort=False)

    remarks = fields.Char('Remarks')
    doses = fields.One2Many('health.immunization_schedule_dose', 'vaccine',
        'Doses')

    def get_rec_name(self, name):
        return (self.vaccine.product.name)

    @staticmethod
    def default_scope():
        return 'systematic'


class ImmunizationSchedule(ModelSQL, ModelView):
    'Immunization Schedule'
    __name__ = 'health.immunization_schedule'

    sched = fields.Char('Code',
        help="Code for this immunization schedule", required=True)
    country = fields.Many2One('country.country','Country')
    year = fields.Integer('Year')
    active = fields.Boolean('Active')
    vaccines = fields.One2Many('health.immunization_schedule_line',
        'sched', 'Vaccines')
    desc = fields.Char('Description',
     help="Short Description for this immunization schedule", required=True)

    def get_rec_name(self, name):
        return (self.sched)

    @staticmethod
    def default_active():
        return True

    @classmethod
    def __setup__(cls):
        super(ImmunizationSchedule, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('sched_uniq', Unique(t, t.sched),
                'The schedule code must be unique'),
        ]


class PathologyCategory(ModelSQL, ModelView):
    'Disease Categories'
    __name__ = 'health.pathology.category'

    name = fields.Char('Category Name', required=True, translate=True)
    parent = fields.Many2One('health.pathology.category', 'Parent Category',
        select=True)
    childs = fields.One2Many(
        'health.pathology.category', 'parent', 'Children Category')

    @classmethod
    def __setup__(cls):
        super(PathologyCategory, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))
        t = cls.__table__()
        cls._sql_constraints += [
            ('name_uniq', Unique(t, t.name),
                'The category name must be unique'),
        ]

    @classmethod
    def validate(cls, categories):
        super(PathologyCategory, cls).validate(categories)
        # FIXME: Must add tree class
        # cls.check_recursion(categories, rec_name='name')

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + ' / ' + self.name
        else:
            return self.name


class PathologyGroup(ModelSQL, ModelView):
    'Pathology Groups'
    __name__ = 'health.pathology.group'

    name = fields.Char('Name', required=True, translate=True)
    code = fields.Char('Code', required=True,
        help='for example MDG6 code will contain the Millennium Development'
        ' Goals # 6 diseases : Tuberculosis, Malaria and HIV/AIDS')
    desc = fields.Char('Short Description', required=True)
    info = fields.Text('Detailed information')
    members = fields.One2Many('health.disease_group.members', 'disease_group',
        'Members', readonly=True)

    @classmethod
    def __setup__(cls):
        super(PathologyGroup, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('code_uniq', Unique(t, t.code),
            'The Pathology Group code must be unique'),
        ]


class Pathology(ModelSQL, ModelView):
    'Health Conditions'
    __name__ = 'health.pathology'

    name = fields.Char('Name', required=True, translate=True,
        help='Health condition name')
    code = fields.Char('Code', required=True,
        help='Specific Code for the Disease (eg, ICD-10)')
    category = fields.Many2One(
        'health.pathology.category', 'Main Category',
        help='Select the main category for this disease This is usually'
        ' associated to the standard. For instance, the chapter on the ICD-10'
        ' will be the main category for de disease')
    groups = fields.One2Many('health.disease_group.members', 'pathology',
        'Groups', help='Specify the groups this pathology belongs. Some '
        'automated processes act upon the code of the group')
    chromosome = fields.Char('Affected Chromosome', help='chromosome number')
    protein = fields.Char(
        'Protein involved', help='Name of the protein(s) affected')
    gene = fields.Char('Gene', help='Name of the gene(s) affected')
    info = fields.Text('Extra Info')
    active = fields.Boolean('Active', select=True)

    @staticmethod
    def default_active():
        return True

    # Include code + description in result
    def get_rec_name(self, name):
        return (self.code + ' : ' + self.name)

    # Search by the health condition code or the description
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]

    @classmethod
    def __setup__(cls):
        super(Pathology, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [(
            'code_uniq', Unique(t, t.code),
            'The disease code must be unique'
        )]


# DISEASE GROUP MEMBERS
class DiseaseMembers(ModelSQL, ModelView):
    'Disease group members'
    __name__ = 'health.disease_group.members'

    pathology = fields.Many2One('health.pathology', 'Condition', readonly=True)
    disease_group = fields.Many2One(
        'health.pathology.group', 'Group', required=True)


class ProcedureCode(ModelSQL, ModelView):
    'Medical Procedures'
    __name__ = 'health.procedure'

    name = fields.Char('Code', required=True)
    description = fields.Char('Long Text', translate=True)
    product = fields.Many2One('product.product', 'Product', required=False,
        domain=[('kind', '=', 'procedure')],
    )

    # Include code + description in result
    def get_rec_name(self, name):
        return (self.name + ' : ' + self.description)

    # Search by the Procedure code or the description
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('name',) + tuple(clause[1:]),
            ('description',) + tuple(clause[1:]),
        ]


class BirthCertExtraInfo (ModelSQL, ModelView):
    'Birth Certificate'
    __name__ = 'health.birth_certificate'

    STATES = {'readonly': Eval('state') == 'done'}

    institution = fields.Many2One('health.institution', 'Institution',
        states=STATES)
    signed_by = fields.Many2One('health.professional', 'Certifier',
        readonly=True, help='Person who certifies this birth document',
        states=STATES)
    certification_date = fields.DateTime('Signed on', readonly=True,
        states=STATES)

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @fields.depends('institution')
    def on_change_institution(self):
        country = None
        subdivision = None
        if (self.institution and self.institution.party.addresses[0].country):
            country = self.institution.party.addresses[0].country.id

        if (self.institution and self.institution.party.addresses[0].subdivision):
            subdivision = self.institution.party.addresses[0].subdivision.id

        self.country = country
        self.country_subdivision = subdivision

    @classmethod
    @ModelView.button
    def sign(cls, certificates):
        pool = Pool()
        Professional = pool.get('health.professional')
        Person = pool.get('party.party')
        party = []

        # Change the state of the birth certificate to "Signed"
        # and write the name of the certifying health professional

        signing_hp = Professional.get_health_professional()
        if not signing_hp:
            raise UserWarning(gettext('msg_health_professional_warning'))

        cls.write(certificates, {
            'state': 'signed',
            'signed_by': signing_hp,
            'certification_date': datetime.now()})

        party.append(certificates[0].name)
        Person.write(party, {
            'birth_certificate': certificates[0].id
        })


class DeathCertExtraInfo (ModelSQL, ModelView):
    'Death Certificate'
    __name__ = 'health.death_certificate'

    STATES = {'readonly': Eval('state') == 'done'}

    institution = fields.Many2One('health.institution', 'Institution',
        states=STATES)
    signed_by = fields.Many2One('health.professional', 'Signed by',
        readonly=True, states={'invisible': Equal(Eval('state'), 'draft')},
        help="Health Professional that signed the death certificate")
    certification_date = fields.DateTime('Certified on', readonly=True)
    cod = fields.Many2One('health.pathology', 'Cause', required=True,
        help="Immediate Cause of Death", states=STATES)
    underlying_conditions = fields.One2Many(
        'health.death_underlying_condition',
        'death_certificate', 'Underlying Conditions', help='Underlying'
        ' conditions that initiated the events resulting in death.'
        ' Please code them in sequential, chronological order',
        states=STATES)

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('institution')
    def on_change_institution(self):
        country = None
        subdivision = None
        if (self.institution and self.institution.party.addresses[0].country):
            country = self.institution.party.addresses[0].country.id

        if (self.institution and self.institution.party.addresses[0].subdivision):
            subdivision = self.institution.party.addresses[0].subdivision.id

        self.country = country
        self.country_subdivision = subdivision

    @classmethod
    @ModelView.button
    def sign(cls, certificates):
        pool = Pool()
        HealthProfessional = pool.get('health.professional')
        Person = pool.get('party.party')

        # Change the state of the death certificate to "Signed"
        # and write the name of the certifying health professional

        # It also set the associated party attribute deceased to True.
        party = []

        signing_hp = HealthProfessional.get_health_professional()
        if not signing_hp:
            raise UserError(gettext("health.msg_health_professional_warning"))

        cls.write(certificates, {
            'state': 'signed',
            'signed_by': signing_hp,
            'certification_date': datetime.now()
        })

        party.append(certificates[0].party)
        Person.write(party, {
            'deceased': True,
            'death_certificate': certificates[0].id
        })


# UNDERLYING CONDITIONS THAT RESULT IN DEATH INCLUDED IN DEATH CERT.
class DeathUnderlyingCondition(ModelSQL, ModelView):
    'Underlying Conditions'
    __name__ = 'health.death_underlying_condition'

    death_certificate = fields.Many2One(
        'health.death_certificate', 'Certificate', readonly=True)
    condition = fields.Many2One(
        'health.pathology', 'Condition', required=True)
    interval = fields.Integer('Interval', help='Approx Interval onset to death',
        required=True)
    unit_of_time = fields.Selection([
        (None, ''),
        ('minutes', 'minutes'),
        ('hours', 'hours'),
        ('days', 'days'),
        ('months', 'months'),
        ('years', 'years'),
        ], 'Unit', select=True, sort=False, required=True)


class InsurancePlan(ModelSQL, ModelView):
    'Insurance Plan'
    __name__ = 'health.insurance.plan'
    _rec_name = 'company'
    active = fields.Boolean('Active')
    company = fields.Many2One('party.party', 'Insurance Company',
        required=True, domain=[('is_insurance_company', '=', True)])
    is_default = fields.Boolean('Default plan',
        help='Check if this is the default plan when assigning this insurance'
        ' company to a patient')
    product = fields.Many2One('product.product', 'Plan', required=True,
        domain=[('kind', '=', 'insurance_plan')],
        help='Insurance company plan')
    notes = fields.Text('Extra info')
    price_list = fields.Many2One('product.price_list', 'Price List',
        help="Use to compute the unit price of lines.")

    @staticmethod
    def default_active():
        return True

    def get_rec_name(self, name):
        return self.product.name


class Insurance(ModelSQL, ModelView):
    'Insurance'
    __name__ = 'health.insurance'
    _rec_name = 'number'

    # Insurance associated to an individual
    party = fields.Many2One('party.party', 'Owner')
    number = fields.Char('Number', required=True)
    company = fields.Many2One('party.party', 'Insurance Company', select=True,
        required=True, domain=[('is_insurance_company', '=', True)])
    member_since = fields.Date('Member since')
    member_exp = fields.Date('Expiration date')
    category = fields.Char('Category', help='Insurance company category')
    insurance_type = fields.Selection([
        (None, ''),
        ('state', 'State'),
        ('labour_union', 'Labour Union / Syndical'),
        ('private', 'Private'),
        ], 'Insurance Type', select=True)
    plan = fields.Many2One('health.insurance.plan', 'Plan',
        help='Insurance company plan', depends=['company'],
        domain=[('company', '=', Eval('company'))],
    )
    notes = fields.Text('Extra Info')

    def get_rec_name(self, name):
        return (self.company.name + ' : ' + self.number)

    @classmethod
    def __setup__(cls):
        super(Insurance, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('number_uniq', Unique(t, t.number, t.company),
                'The number must be unique per insurance company'),
        ]


class BirthCertificate (ModelSQL, ModelView):
    'Birth Certificate'
    __name__ = 'health.birth_certificate'

    STATES = {'readonly': Eval('state') == 'done'}
    party = fields.Many2One('party.party', 'Person', required=True,
        domain=[('is_person', '=', True)],
        states={'readonly': Eval('id', 0) > 0})
    mother = fields.Many2One('party.party', 'Mother',
        domain=[('is_person', '=', True)], states=STATES)
    father = fields.Many2One('party.party', 'Father',
        domain=[('is_person', '=', True)], states=STATES)
    code = fields.Char('Code', required=True, states=STATES)
    birthday = fields.Date('Date of Birth', required=True, states=STATES)
    institution = fields.Many2One('health.institution', 'Institution',
        states=STATES, help='Health Care Institution')
    observations = fields.Text('Observations', states=STATES)
    country = fields.Many2One('country.country', 'Country', required=True,
        states=STATES)
    country_subdivision = fields.Many2One('country.subdivision', 'Subdivision',
        domain=[('country', '=', Eval('country'))],
        depends=['country'], states=STATES)
    state = fields.Selection([
        (None, ''),
        ('draft', 'Draft'),
        ('signed', 'Signed'),
        ('done', 'Done'),
        ], 'State', readonly=True, sort=False)

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('party')
    def on_change_with_birthday(self):
        if (self.party and self.party.birthday):
            return self.party.birthday

    @classmethod
    def __setup__(cls):
        super(BirthCertificate, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.party), 'Certificate already exists !'),
            ('code_uniq', Unique(t, t.code), 'Certificate already exists !'),
        ]
        cls._buttons.update({
            'sign': {'invisible': Or(Equal(Eval('state'), 'signed'),
                Equal(Eval('state'), 'done'))}
        })

    @classmethod
    def validate(cls, certificates):
        super(BirthCertificate, cls).validate(certificates)
        for certificate in certificates:
            certificate.validate_dob()

    def validate_dob(self):
        if (self.party.birthday != self.birthday):
            raise UserError(gettext(
                "health.msg_date_party_differs_certificate"
            ))


class DeathCertificate (ModelSQL, ModelView):
    'Death Certificate'
    __name__ = 'health.death_certificate'

    STATES = {'readonly': Eval('state') == 'done'}

    party = fields.Many2One('party.party', 'Person', required=True,
        domain=[('is_person', '=', True)], states=STATES)
    code = fields.Char('Code', required=True, states=STATES)
    autopsy = fields.Boolean('Autopsy', help="Check this box "
        "if autopsy has been done", states=STATES)
    dod = fields.DateTime('Date', required=True, help="Date and time of Death",
        states=STATES)
    type_of_death = fields.Selection([
            (None, ''),
            ('natural', 'Natural'),
            ('suicide', 'Suicide'),
            ('homicide', 'Homicide'),
            ('undetermined', 'Undetermined'),
            ('pending_investigation', 'Pending Investigation'),
        ], 'Type of death', required=True, sort=False,
        states=STATES)
    place_of_death = fields.Selection([
            (None, ''),
            ('home', 'Home'),
            ('work', 'Work'),
            ('public_place', 'Public place'),
            ('health_center', 'Health Center'),
        ], 'Place', required=True, sort=False, states=STATES)
    operational_sector = fields.Many2One('health.operational_sector',
        'Op. Sector', states=STATES)
    du = fields.Many2One('health.du', 'DU', help="Domiciliary Unit",
        states=STATES)
    place_details = fields.Char('Details', states=STATES)
    country = fields.Many2One('country.country', 'Country', required=True,
        states=STATES)
    country_subdivision = fields.Many2One(
        'country.subdivision', 'Subdivision',
        domain=[('country', '=', Eval('country'))],
        depends=['country'], states=STATES)
    age = fields.Function(fields.Char('Age'), 'get_age_at_death')
    observations = fields.Text('Observations', states=STATES)
    state = fields.Selection([
        (None, ''),
        ('draft', 'Draft'),
        ('signed', 'Signed'),
        ('done', 'Done'),
        ], 'State', readonly=True, sort=False)

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def __setup__(cls):
        super(DeathCertificate, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.party), 'Certificate already exists !'),
            ('code_uniq', Unique(t, t.code), 'Certificate already exists !'),
        ]

        cls._buttons.update({
            'sign': {'invisible': Or(Equal(Eval('state'), 'signed'),
                Equal(Eval('state'), 'done'))}
            })

    def get_age_at_death(self, name):
        if (self.party.birthday):
            delta = relativedelta(self.dod, self.party.birthday)
            years_months_days = str(delta.years) + 'y ' \
                + str(delta.months) + 'm ' \
                + str(delta.days) + 'd'
        else:
            years_months_days = None
        return years_months_days


class PatientData(ModelSQL, ModelView):
    'Patient related information'
    __name__ = 'health.patient'

    party = fields.Many2One('party.party', 'Patient', required=True,
        domain=[
            ('is_patient', '=', True),
            ('is_person', '=', True),
        ],
        states={'readonly': Eval('id', 0) > 0})
    # lastname = fields.Function(
    #     fields.Char('Lastname'), 'get_patient_lastname',
    #     searcher='search_patient_lastname')
    puid = fields.Function(fields.Char('PUID', help="Person Unique Identifier"),
        'get_patient_puid', searcher='search_patient_puid')
    family = fields.Many2One('health.family', 'Family', help='Family Code')
    current_insurance = fields.Many2One(
        'health.insurance', 'Insurance',
        domain=[('party', '=', Eval('party'))],
        depends=['party'],
        help='Insurance information. You may choose from the different'
        ' insurances belonging to the patient')
    current_address = fields.Many2One('party.address', 'Temp. Addr',
        domain=[('party', '=', Eval('party'))],
        depends=['party'],
        help="Use this address for temporary contact information. For example \
        if the patient is on vacation, you can put the hotel address. \
        In the case of a Domiciliary Unit, just link it to the name of the \
        contact in the address form.")
    primary_care_doctor = fields.Many2One(
        'health.professional',
        'GP', help='Current General Practitioner / Family Doctor')
    photo = fields.Function(fields.Binary('Picture'), 'get_patient_photo')
    birthday = fields.Function(fields.Date('DoB'), 'get_patient_birthday')
    age = fields.Function(fields.Char('Age'), 'get_patient_age')
    sex = fields.Function(fields.Selection([
        (None, ''),
        ('male', 'Male'),
        ('female', 'Female'),
        ], 'Sex'), 'get_patient_sex')
    marital_status = fields.Function(
        fields.Selection([
            (None, ''),
            ('free_union', 'Free Union'),
            ('married', 'Married'),
            ('single', 'Single'),
            ('divorced', 'Divorced'),
            ('widow', 'Widow'),
            ], 'Marital Status'),
            'get_patient_marital_status')
    blood_type = fields.Selection([
        (None, ''),
        ('A', 'A'),
        ('B', 'B'),
        ('AB', 'AB'),
        ('O', 'O'),
        ], 'Blood Type', sort=False)
    rh = fields.Selection([
        (None, ''),
        ('+', '+'),
        ('-', '-'),
        ], 'Rh')
    vaccinations = fields.One2Many('health.vaccination', 'patient',
        'Vaccinations', readonly=True)
    medications = fields.One2Many('health.patient.medication', 'patient',
        'Medications')
    diseases = fields.One2Many('health.patient.disease', 'patient',
        'Conditions', readonly=True)
    critical_summary = fields.Function(fields.Text(
        'Important health conditions related to this patient',
        help='Automated summary of patient important health conditions '
        'other critical information'),
        'patient_critical_summary')
    critical_info = fields.Text(
        'Free text information not included in the automatic summary',
        help='Write any important information on the patient\'s condition,'
        ' surgeries, allergies, ...')
    general_info = fields.Text('General Information',
        help='General information about the patient')
    deceased = fields.Function(fields.Boolean('Deceased'),
        'check_is_alive')
    dod = fields.Function(fields.DateTime('Date of Death',
        states={
            'invisible': Not(Bool(Eval('deceased'))),
            },
        depends=['deceased']), 'get_dod')
    cod = fields.Function(fields.Many2One('health.pathology', 'Cause of Death',
        states={
            'invisible': Not(Bool(Eval('deceased'))),
            },
        depends=['deceased']), 'get_cod')
    childbearing_age = fields.Function(
        fields.Boolean('Potential for Childbearing'), 'get_childbearing_age')
    appointments = fields.One2Many('health.appointment', 'patient',
        'Appointments')
    active = fields.Boolean('Active', select=True)
    disabled_person = fields.Boolean('Disabled Person')
    disability_kind = fields.Char('Disability Kind', states={
        'invisible': ~Eval('disabled_person')
    })
    familiar_antecedents = fields.Text('Familiar Antecedents')
    #Antecedents / Toxic Habits
    disease_history = fields.Text('Disease History')
    childhood_history = fields.Text('Childhood')
    adolescence_history = fields.Text('Adolescence')
    adulthood_history = fields.Text('Adulthood')
    family_history = fields.Text('Family History')
    obstetrics_gynecology = fields.Boolean('Obstetrics Gynecology')
    date_last_menstruation = fields.Date('DLM', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    }, help='Date of last menstruation')
    births = fields.Integer('Births', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    abortions = fields.Integer('Abortions', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    caesarean = fields.Integer('Caesarean', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    pregnancies = fields.Integer('Pregnancies', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology')),
    })
    t_gestations = fields.Integer('T. Gestations', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    common_medication = fields.Text('Common Medication')
    blood_transfusion = fields.Text('Blood Transfusion')
    #toxic_habits = fields.One2Many('health.emergency.toxic_habits',
        #'emergency', 'Toxic Habits', states=STATES)
    coffee = fields.Selection(YES_NO, 'Coffee')
    coffee_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('coffee', '') != 'yes')
    })
    smoke = fields.Selection(YES_NO, 'Smoke')
    smoke_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('smoke', '') != 'yes')
    })
    alcohol = fields.Selection(YES_NO, 'Alcohol')
    alcohol_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('alcohol', '') != 'yes')
    })
    other_habits = fields.Char('Other Habits', help="For example drugs consume")
    medications_allergy = fields.Text('Medications Allegy')
    food_allergy = fields.Text('Food Allegy')
    other_allergy = fields.Text('Other Allegy')
    surgical_history = fields.Text('Surgical')
    trauma_history = fields.Text('Trauma')

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_alcohol():
        return 'no'

    @staticmethod
    def default_smoke():
        return 'no'

    @staticmethod
    def default_coffee():
        return 'no'

    @classmethod
    def __setup__(cls):
        super(PatientData, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.party), 'The Patient already exists !'),
        ]

    def patient_critical_summary(self, name):
        # Patient Critical Information Summary
        # The information will be shown in the front page

        allergies = ""
        other_conditions = ""
        conditions = []
        for disease in self.diseases:
            for member in disease.pathology.groups:
                '''Retrieve patient allergies'''
                if (member.disease_group.name == "ALLERGIC"):
                    if disease.pathology.name not in conditions:
                        allergies = allergies + disease.pathology.rec_name + "\n"
                        conditions.append(disease.pathology.rec_name)

            '''Retrieve patient other relevant conditions '''
            '''Chronic and active'''
            if (disease.status == "c" or disease.is_active):
                if disease.pathology.name not in conditions:
                            other_conditions = other_conditions + disease.pathology.rec_name + "\n"

        return allergies + other_conditions

    def get_patient_birthday(self, name):
        if self.party:
            return self.party.birthday

    def get_patient_sex(self, name):
        if self.party:
            return self.party.sex

    def get_patient_photo(self, name):
        return self.party.photo

    def get_patient_puid(self, name):
        return self.party.puid

    def get_patient_marital_status(self, name):
        return self.party.marital_status

    def check_is_alive(self, name):
        return self.party.deceased

    def get_patient_age(self, name):
        res = self.party.age
        if res:
            return str(res)

    def get_childbearing_age(self, name):
        return compute_age_from_dates(self.birthday, self.deceased,
            self.dod, self.sex, name, None)

    def get_dod(self, name):
        if (self.deceased):
            return self.party.death_certificate.dod

    def get_cod(self, name):
        if (self.deceased):
            return self.party.death_certificate.cod.id

    @fields.depends('name')
    def on_change_name(self):
        if self.party:
            self.sex = self.party.sex
            self.age = self.party.age

    @classmethod
    def search_patient_puid(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('party.puid', clause[1], value))
        return res

    # def get_patient_lastname(self, name):
    #     return self.party.lastname

    # @classmethod
    # def search_patient_lastname(cls, name, clause):
    #     res = []
    #     value = clause[2]
    #     res.append(('party.lastname', clause[1], value))
    #     return res

    def get_rec_name(self, name):
        return self.party.name

    # Search by the patient name, lastname or PUID
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [
            bool_op,
            ('puid',) + tuple(clause[1:]),
            ('party',) + tuple(clause[1:]),
            ('party.id_number',) + tuple(clause[1:]),
            # ('lastname',) + tuple(clause[1:]),
        ]


# PATIENT CONDITIONS INFORMATION
class PatientDiseaseInfo(ModelSQL, ModelView):
    'Patient Conditions History'
    __name__ = 'health.patient.disease'
    patient = fields.Many2One('health.patient', 'Patient')
    pathology = fields.Many2One(
        'health.pathology', 'Condition', required=True, help='Condition')
    disease_severity = fields.Selection([
        (None, ''),
        ('1_mi', 'Mild'),
        ('2_mo', 'Moderate'),
        ('3_sv', 'Severe'),
        ], 'Severity', select=True, sort=False)
    is_on_treatment = fields.Boolean('Currently on Treatment')
    is_infectious = fields.Boolean(
        'Infectious Disease',
        help='Check if the patient has an infectious / transmissible disease')
    short_comment = fields.Char(
        'Remarks',
        help='Brief, one-line remark of the disease. Longer description will'
        ' go on the Extra info field')
    healthprof = fields.Many2One('health.professional', 'Health Prof',
        help='Health Professional who treated or diagnosed the patient')
    diagnosed_date = fields.Date('Date of Diagnosis')
    healed_date = fields.Date('Healed')
    is_active = fields.Boolean('Active disease')
    age = fields.Integer(
        'Age when diagnosed',
        help='Patient age at the moment of the diagnosis. Can be estimative')
    pregnancy_warning = fields.Boolean('Pregnancy warning')
    weeks_of_pregnancy = fields.Integer('Contracted in pregnancy week #')
    is_allergy = fields.Boolean('Allergic Disease')
    allergy_type = fields.Selection([
        (None, ''),
        ('da', 'Drug Allergy'),
        ('fa', 'Food Allergy'),
        ('ma', 'Misc Allergy'),
        ('mc', 'Misc Contraindication'),
        ], 'Allergy type', select=True, sort=False)
    pcs_code = fields.Many2One('health.procedure', 'Code',
        help='Procedure code, for example, ICD-10-PCS Code 7-character string')
    treatment_description = fields.Char('Treatment Description')
    date_start_treatment = fields.Date('Start', help='Start of treatment date')
    date_stop_treatment = fields.Date('End', help='End of treatment date')
    status = fields.Selection([
        (None, ''),
        ('a', 'acute'),
        ('c', 'chronic'),
        ('u', 'unchanged'),
        ('h', 'healed'),
        ('i', 'improving'),
        ('w', 'worsening'),
        ], 'Status', select=True, sort=False)
    extra_info = fields.Text('Extra Info')
    healthprof = fields.Many2One('health.professional', 'Health Prof',
        readonly=True)
    related_evaluations = fields.One2Many('health.patient.evaluation',
        'related_condition', 'Related Evaluations', readonly=True)
    # Show warning on infectious disease
    infectious_disease_icon = fields.Function(
        fields.Char('Infect'), 'get_infect_disease_icon'
    )

    @classmethod
    def __setup__(cls):
        super(PatientDiseaseInfo, cls).__setup__()
        cls._order.insert(0, ('is_active', 'DESC'))
        cls._order.insert(1, ('disease_severity', 'DESC'))
        cls._order.insert(2, ('is_infectious', 'DESC'))
        cls._order.insert(3, ('diagnosed_date', 'DESC'))

    @staticmethod
    def default_is_active():
        return True

    @classmethod
    def validate(cls, diseases):
        super(PatientDiseaseInfo, cls).validate(diseases)
        for disease in diseases:
            disease.validate_disease_period()
            disease.validate_treatment_dates()

    def validate_disease_period(self):
        Lang = Pool().get('ir.lang')
        lang, = Lang.search([
            ('code', '=', Transaction().language),
        ])
        if (self.healed_date and self.diagnosed_date):
            if (self.healed_date < self.diagnosed_date):
                raise UserError(gettext('health.msg_healed_date_before_diagnosed_date',
                    healed_date=Lang.strftime(self.healed_date,
                        lang.code, lang.date),
                    diagnosed_date=Lang.strftime(self.diagnosed_date,
                        lang.code, lang.date),
                ))

    def validate_treatment_dates(self):
        Lang = Pool().get('ir.lang')
        lang, = Lang.search([
            ('code', '=', Transaction().language),
        ])
        if (self.date_stop_treatment and self.date_start_treatment):
            if (self.date_stop_treatment < self.date_start_treatment):
                raise UserError(gettext('health.msg_end_treatment_date_before_start',
                    date_stop_treatment=Lang.strftime(
                        self.date_stop_treatment,
                        lang.code, lang.date),
                    date_start_treatment=Lang.strftime(
                        self.date_start_treatment,
                        lang.code, lang.date),
                ))

    def get_infect_disease_icon(self, name):
        if (self.is_infectious):
            return 'health-warning'

    @staticmethod
    def default_healthprof():
        HealthProfessional = Pool().get('health.professional')
        return HealthProfessional.get_health_professional()

    def get_rec_name(self, name):
        return self.pathology.rec_name


class Appointment(ModelSQL, ModelView):
    'Patient Appointments'
    __name__ = 'health.appointment'
    number = fields.Char('Appointment ID', readonly=True)
    healthprof = fields.Many2One('health.professional', 'Health Prof',
        select=True, help='Health Professional')
    patient = fields.Many2One('health.patient', 'Patient',
        states={'required': (Eval('state') != 'free')})
    appointment_date = fields.DateTime('Date and Time')
    checked_in_date = fields.DateTime('Checked-in Time')
    institution = fields.Many2One('health.institution', 'Institution',
        help='Health Care Institution')
    speciality = fields.Many2One('health.specialty', 'Specialty',
        help='Medical Specialty / Sector')
    state = fields.Selection([
        (None, ''),
        ('free', 'Free'),
        ('confirmed', 'Confirmed'),
        ('checked_in', 'Checked in'),
        ('done', 'Done'),
        ('user_cancelled', 'Cancelled by patient'),
        ('center_cancelled', 'Cancelled by Health Center'),
        ('no_show', 'No show')
        ], 'State', sort=False)
    urgency = fields.Selection([
        (None, ''),
        ('a', 'Normal'),
        ('b', 'Urgent'),
        ('c', 'Medical Emergency'),
        ('d', 'Priority'),
        ], 'Urgency', sort=False)
    comments = fields.Text('Comments')
    appointment_type = fields.Selection([
        ('outpatient', 'Outpatient'),
        ('inpatient', 'Inpatient'),
        ], 'Type', sort=False)
    visit_type = fields.Selection([
        ('', ''),
        ('other', 'Other'),
        ('new', 'New health condition'),
        ('followup', 'Followup'),
        ('well_child', 'Well Child visit'),
        ('well_woman', 'Well Woman visit'),
        ('well_man', 'Well Man visit'),
        ], 'Visit', sort=False)
    product = fields.Many2One('product.product', 'Product',
        domain=[('type', '=', 'service')], help='Consultation service')

    @classmethod
    def __setup__(cls):
        super(Appointment, cls).__setup__()
        cls._order.insert(0, ('appointment_date', 'ASC'))
        cls._buttons.update({
            'check_in': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
        })
        cls._buttons.update({
            'no_show': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
        })

    @classmethod
    @ModelView.button
    def check_in(cls, records):
        cls.write(records, {'state': 'checked_in'})

    @classmethod
    @ModelView.button
    def no_show(cls, records):
        cls.write(records, {'state': 'no_show'})

    @classmethod
    def create(cls, vlist):
        config = Pool().get('health.configuration').get_config()

        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if values['state'] == 'confirmed' and not values.get('number'):
                values['number'] = config.appointment_sequence.get()
        return super(Appointment, cls).create(vlist)

    @classmethod
    def write(cls, appointments, values):
        config = Pool().get('health.configuration').get_config()

        for appointment in appointments:
            if values.get('state') == 'confirmed' and not values.get('number'):
                values['number'] = config.appointment_sequence.get()

            #Update the checked-in time only if unset
            if values.get('state') == 'checked_in' \
                    and values.get('checked_in_date') is None:
                values['checked_in_date'] = datetime.now()

        return super(Appointment, cls).write(appointments, values)

    @classmethod
    def copy(cls, appointments, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['number'] = None
        default['appointment_date'] = cls.default_appointment_date()
        default['state'] = cls.default_state()
        return super(Appointment, cls).copy(appointments, default=default)

    @staticmethod
    def default_urgency():
        return 'a'

    @staticmethod
    def default_appointment_date():
        return datetime.now()

    @staticmethod
    def default_appointment_type():
        return 'outpatient'

    @staticmethod
    def default_visit_type():
        return 'new'

    @staticmethod
    def default_state():
        return 'confirmed'

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @fields.depends('patient')
    def on_change_patient(self):
        if self.patient:
            self.state = 'confirmed'

    @fields.depends('healthprof')
    def on_change_with_speciality(self):
        # Return the Current / Main speciality of the Health Professional
        # if this speciality has been specified in the HP record.
        if (self.healthprof and self.healthprof.main_specialty):
            return self.healthprof.main_specialty.specialty.id

    # @staticmethod
    # def default_speciality():
    #     # This method will assign the Main specialty to the appointment
    #     # if there is a health professional associated to the login user
    #     # as a default value.
    #     # It will be overwritten if the health professional is modified in
    #     # this view, the on_change_with will take effect.
    #
    #     HealthProfessional = Pool().get('health.professional')
    #
    #     # Get Party ID associated to the Health Professional
    #     hp_party_id = HealthProfessional.get_health_professional()
    #
    #     if hp_party_id:
    #         # Retrieve the health professional Main specialty, if assigned
    #         health_professional = HealthProfessional.search(
    #             [('id', '=', hp_party_id)], limit=1)[0]
    #         hp_main_specialty = health_professional.main_specialty
    #
    #         if hp_main_specialty:
    #             return hp_main_specialty.specialty.id

    def get_rec_name(self, name):
        return self.number


class AppointmentReport(ModelSQL, ModelView):
    'Appointment Report'
    __name__ = 'health.appointment.report'

    f = fields.Char('PUID')
    patient = fields.Many2One('health.patient', 'Patient')
    healthprof = fields.Many2One('health.professional', 'Health Prof')
    age = fields.Function(fields.Char('Age'), 'get_patient_age')
    sex = fields.Selection([
        (None, ''),
        ('m', 'Male'),
        ('f', 'Female')], 'Sex')
    address = fields.Function(fields.Char('Address'), 'get_address')
    insurance = fields.Function(fields.Char('Insurance'), 'get_insurance')
    appointment_date = fields.Date('Date')
    appointment_date_time = fields.DateTime('Date and Time')
    diagnosis = fields.Function(
        fields.Many2One('health.pathology', 'Main Codition'), 'get_diagnosis')

    @classmethod
    def __setup__(cls):
        super(AppointmentReport, cls).__setup__()
        cls._order.insert(0, ('appointment_date_time', 'ASC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        appointment = pool.get('health.appointment').__table__()
        party = pool.get('party.party').__table__()
        patient = pool.get('health.patient').__table__()
        join1 = Join(appointment, patient)
        join1.condition = join1.right.id == appointment.patient
        join2 = Join(join1, party)
        join2.condition = join2.right.id == join1.right.name
        where = Literal(True)
        if Transaction().context.get('date_start'):
            where &= (appointment.appointment_date >=
                    Transaction().context['date_start'])
        if Transaction().context.get('date_end'):
            where &= (appointment.appointment_date <
                    Transaction().context['date_end'] + timedelta(days=1))
        if Transaction().context.get('healthprof'):
            where &= \
                appointment.healthprof == Transaction().context['healthprof']

        return join2.select(
            appointment.id,
            appointment.create_uid,
            appointment.create_date,
            appointment.write_uid,
            appointment.write_date,
            join2.right.puid,
            join1.right.id.as_('patient'),
            join2.right.sex,
            appointment.appointment_date,
            appointment.appointment_date.as_('appointment_date_time'),
            appointment.healthprof,
            where=where)

    def get_address(self, name):
        res = ''
        if self.patient.party.addresses:
            res = self.patient.party.addresses[0].full_address
        return res

    def get_insurance(self, name):
        res = ''
        if self.patient.current_insurance:
            res = self.patient.current_insurance.company.party
        return res

    def get_diagnosis(self, name):
        Evaluation = Pool().get('health.patient.evaluation')

        res = None
        evaluations = Evaluation.search([
            ('origin', '=', str(self))
        ])
        if evaluations:
            evaluation = evaluations[0]
            if evaluation.diagnosis:
                res = evaluation.diagnosis.id
        return res

    def get_patient_age(self, name):
        return str(self.patient.party.age)


class OpenAppointmentReportStart(ModelView):
    'Open Appointment Report'
    __name__ = 'health.appointment.report.open.start'
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    healthprof = fields.Many2One('health.professional', 'Health Prof')

    @staticmethod
    def default_date_start():
        return datetime.now()

    @staticmethod
    def default_date_end():
        return datetime.now()

    @staticmethod
    def default_healthprof():
        return Pool().get('health.professional').get_health_professional()


class OpenAppointmentReport(Wizard):
    'Open Appointment Report'
    __name__ = 'health.appointment.report.open'

    start = StateView(
        'health.appointment.report.open.start',
        'health.appointments_report_open_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Open', 'open_', 'tryton-ok', default=True),
        ])
    open_ = StateAction('health.act_appointments_report_view_tree')

    def do_open_(self, action):
        health_prof = ''
        if self.start.healthprof:
            health_prof = self.start.healthprof.id

        action['pyson_context'] = PYSONEncoder().encode({
            'date_start': self.start.date_start,
            'date_end': self.start.date_end,
            'healthprof': health_prof,
        })

        # Show action name depending on whether is an specific hp
        # or for all professionals

        if health_prof:
            action['name'] += ' - %s' % (self.start.healthprof.rec_name)
        else:
            action['name'] += ' - %s' % "All Health Professionals"

        return action, {}

    def transition_open_(self):
        return 'end'


# PATIENT MEDICATION TREATMENT
class PatientMedication(ModelSQL, ModelView):
    'Patient Medication'
    __name__ = 'health.patient.medication'
    medicament = fields.Many2One('health.medicament', 'Medicament',
        required=True, help='Prescribed Medicament')
    indication = fields.Many2One('health.pathology', 'Indication',
        help='Choose a disease for this medicament from the disease list. It'
        ' can be an existing disease of the patient or a prophylactic.')
    patient = fields.Many2One('health.patient', 'Patient', readonly=True)
    healthprof = fields.Many2One(
        'health.professional', 'Health Prof', readonly=True,
        help='Health Professional who prescribed or reviewed the medicament')

    institution = fields.Many2One('health.institution', 'Institution')
    is_active = fields.Boolean('Active',
        help='Check if the patient is currently taking the medication')
    discontinued = fields.Boolean('Discontinued')
    course_completed = fields.Boolean('Course Completed')
    discontinued_reason = fields.Char('Reason for discontinuation',
        states={
            'invisible': Not(Bool(Eval('discontinued'))),
            'required': Bool(Eval('discontinued')),
        },
        depends=['discontinued'],
        help='Short description for discontinuing the treatment',)
    adverse_reaction = fields.Text(
        'Adverse Reactions',
        help='Side effects or adverse reactions that the patient experienced')
    notes = fields.Text('Extra Info')
    start_treatment = fields.DateTime('Start',
        help='Date of start of Treatment')
    end_treatment = fields.DateTime('End', help='Date of start of Treatment')
    dose = fields.Float('Dose',
        help='Amount of medication (eg, 250 mg) per dose')
    dose_unit = fields.Many2One('health.dose.unit', 'dose unit',
        help='Unit of measure for the medication to be taken')
    route = fields.Many2One('health.drug.route', 'Administration Route',
        help='Drug administration route code.')
    form = fields.Many2One('health.drug.form', 'Form',
        help='Drug form, such as tablet or gel')
    qty = fields.Integer('x',
        help='Quantity of units (eg, 2 capsules) of the medicament')

    duration = fields.Integer('Treatment duration',
        help='Period that the patient must take the medication. in minutes,'
        ' hours, days, months, years or indefinately')
    duration_period = fields.Selection([
        (None, ''),
        ('minutes', 'minutes'),
        ('hours', 'hours'),
        ('days', 'days'),
        ('months', 'months'),
        ('years', 'years'),
        ('indefinite', 'indefinite'),
        ], 'Treatment period', sort=False,
        help='Period that the patient must take the medication in minutes,'
        ' hours, days, months, years or indefinately')
    common_dosage = fields.Many2One(
        'health.medication.dosage', 'Frequency',
        help='Common / standard dosage frequency for this medicament')
    admin_times = fields.Char(
        'Admin hours',
        help='Suggested administration hours. For example, at 08:00, 13:00'
        ' and 18:00 can be encoded like 08 13 18')
    frequency = fields.Integer(
        'Frequency',
        help='Time in between doses the patient must wait (ie, for 1 pill'
        ' each 8 hours, put here 8 and select \"hours\" in the unit field')
    frequency_unit = fields.Selection([
        (None, ''),
        ('seconds', 'seconds'),
        ('minutes', 'minutes'),
        ('hours', 'hours'),
        ('days', 'days'),
        ('weeks', 'weeks'),
        ('wr', 'when required'),
        ], 'unit', select=True, sort=False)
    frequency_prn = fields.Boolean(
        'PRN', help='Use it as needed, pro re nata')
    infusion = fields.Boolean('Infusion',
        help='Mark if the medication is in the form of infusion' \
        ' Intravenous, Gastrostomy tube, nasogastric, etc...' )
    infusion_rate = fields.Float('Rate',
            states={'invisible': Not(Bool(Eval('infusion')))})
    infusion_rate_units = fields.Selection([
        (None, ''),
        ('mlhr', 'mL/hour'),
        ], 'Unit Rate',
        states={'invisible': Not(Bool(Eval('infusion')))},
        select=True, sort=False)
    prescription = fields.Many2One('health.prescription.order',
        'Prescription', readonly=True, depends=['patient'],
        domain=[('patient', '=', Eval('patient'))],
        help='Related prescription')

    @classmethod
    def __setup__(cls):
        super(PatientMedication, cls).__setup__()
        cls._order.insert(0, ('is_active', 'DESC'))
        cls._order.insert(1, ('start_treatment', 'DESC'))

    @fields.depends('discontinued', 'course_completed')
    def on_change_with_is_active(self):
        return not (self.discontinued or self.course_completed)

    @fields.depends('is_active', 'course_completed')
    def on_change_with_discontinued(self):
        return not (self.is_active or self.course_completed)

    @fields.depends('is_active', 'discontinued')
    def on_change_with_course_completed(self):
        return not (self.is_active or self.discontinued)

    @staticmethod
    def default_is_active():
        return True

    @staticmethod
    def default_frequency_unit():
        return 'hours'

    @staticmethod
    def default_duration_period():
        return 'days'

    @staticmethod
    def default_qty():
        return 1

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_healthprof():
        return Pool().get('health.professional').get_health_professional()

    @classmethod
    def validate(cls, medications):
        super(PatientMedication, cls).validate(medications)
        for medication in medications:
            medication.validate_medication_dates()

    def validate_medication_dates(self):
        Lang = Pool().get('ir.lang')

        lang, = Lang.search([('code', '=', Transaction().language)])
        if self.end_treatment:
            if (self.end_treatment < self.start_treatment):
                raise UserError(gettext('health.msg_end_date_before_start',
                    start_treatment=Lang.strftime(
                        self.start_treatment, lang.code, lang.date
                    ),
                    end_treatment=Lang.strftime(
                        self.end_treatment, lang.code, lang.date),
                    )
                )


class PatientVaccination(ModelSQL, ModelView):
    'Patient Vaccination information'
    __name__ = 'health.vaccination'
    patient = fields.Many2One('health.patient', 'Patient', required=True)
    vaccine = fields.Many2One('health.medicament', 'Vaccine', required=True,
        domain=[('product.kind', '=', 'vaccine')],
        help='Vaccine Name. Make sure that the vaccine has all the'
        ' proper information at product level. Information such as provider,'
        ' supplier code, tracking number, etc.. This  information must always'
        ' be present. If available, please copy / scan the vaccine leaflet'
        ' and attach it to this record')
    admin_route = fields.Selection([
        (None, ''),
        ('im', 'Intramuscular'),
        ('sc', 'Subcutaneous'),
        ('id', 'Intradermal'),
        ('nas', 'Intranasal'),
        ('po', 'Oral'),
        ], 'Route', sort=False)
    picture = fields.Binary('Label')
    vaccine_expiration_date = fields.Date('Expiration date')
    vaccine_lot = fields.Char('Lot Number',
        help='Please check on the vaccine (product) production lot numberand'
        ' tracking number when available !')
    institution = fields.Many2One('health.institution', 'Institution',
        help='Medical Center where the patient is being or was vaccinated')
    date = fields.DateTime('Date')
    dose = fields.Integer('Dose #')
    next_dose_date = fields.DateTime('Next Dose')
    observations = fields.Text('Observations')
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One(
        'health.professional', 'Health Prof', readonly=True,
        help="Health Professional who administered or reviewed the vaccine \
         information")
    signed_by = fields.Many2One(
        'health.professional', 'Signed by', readonly=True,
        states={'invisible': Equal(Eval('state'), 'in_progress')},
        help="Health Professional that signed the vaccination document")
    amount = fields.Float('Amount',
        help='Amount of vaccine administered, in mL . The dose per mL \
            (eg, mcg, EL.U ..) can be found at the related medicament')
    admin_site = fields.Selection([
        (None, ''),
        ('lvl', 'left vastus lateralis'),
        ('rvl', 'right vastus lateralis'),
        ('ld', 'left deltoid'),
        ('rd', 'right deltoid'),
        ('lalt', 'left anterolateral fat of thigh'),
        ('ralt', 'right anterolateral fat of thigh'),
        ('lpua', 'left posterolateral fat of upper arm'),
        ('rpua', 'right posterolateral fat of upper arm'),
        ('lfa', 'left fore arm'),
        ('rfa', 'right fore arm')], 'Admin Site')
    state = fields.Selection([
        (None, ''),
        ('in_progress', 'In Progress'),
        ('done', 'Done'),
        ], 'State', readonly=True)

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_healthprof():
        return Pool().get('health.professional').get_health_professional()

    @staticmethod
    def default_state():
        return 'in_progress'

    @staticmethod
    def default_date():
        return datetime.now()

    @staticmethod
    def default_dose():
        return 1

    @classmethod
    def __setup__(cls):
        super(PatientVaccination, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('dose_uniq', Unique(t, t.patient, t.vaccine, t.dose),
                'This vaccine dose has been given already to the patient'),
            ]
        cls._buttons.update({
            'sign': {'invisible': Equal(Eval('state'), 'done')}
        })

    @classmethod
    @ModelView.button
    def sign(cls, vaccinations):
        # Change the state of the vaccination to "Done"
        # and write the name of the signing health professional
        HealthProfessional = Pool().get('health.professional')
        signing_hp = HealthProfessional.get_health_professional()
        if not signing_hp:
            raise UserError(gettext('msg_no_professional_associated_user'))
        cls.write(vaccinations, {
            'state': 'done',
            'signed_by': signing_hp
        })

    @classmethod
    def write(cls, vaccinations, vals):
        # Don't allow to modify the record if the vaccination has been signed
        if vaccinations[0].state == 'done':
            raise UserError(gettext('health.msg_vaccination_can_no_modify'))
        return super(PatientVaccination, cls).write(vaccinations, vals)

    @classmethod
    def validate(cls, vaccines):
        super(PatientVaccination, cls).validate(vaccines)
        for vaccine in vaccines:
            vaccine.validate_next_dose_date()

    def validate_next_dose_date(self):
        if (self.next_dose_date):
            if (self.next_dose_date < self.date):
                raise UserError(gettext('next_dose_before_first'))


class PatientPrescriptionOrder(ModelSQL, ModelView):
    'Prescription Order'
    __name__ = 'health.prescription.order'
    _rec_name = 'prescription_id'
    STATES = {'readonly': Not(Eval('state') == 'draft')}

    patient = fields.Many2One('health.patient', 'Patient', required=True,
        states=STATES)
    prescription_id = fields.Char('Prescription ID', readonly=True,
        help='Type in the ID of this prescription')
    prescription_date = fields.DateTime('Prescription Date', states=STATES)
    user = fields.Many2One('res.user', 'Prescribing Doctor', readonly=True)
    origin = fields.Reference('Origin', selection='get_origin', select=True,
        states={'readonly': True})
    pharmacy = fields.Many2One('party.party', 'Pharmacy',
        domain=[('is_pharmacy', '=', True)],
        states={
            'readonly': (Eval('state') != 'draft') & Bool(Eval('pharmacy')),
        },
        depends=['state'])
    lines = fields.One2Many('health.prescription.line', 'order',
        'Prescription line', states=STATES)
    notes = fields.Text('Prescription Notes', states=STATES)
    pregnancy_warning = fields.Boolean('Pregnancy Warning', readonly=True)
    prescription_warning_ack = fields.Boolean('Prescription verified',
        states=STATES)
    healthprof = fields.Many2One('health.professional', 'Prescribed by',
        readonly=True)
    report_prescription_date = fields.Function(fields.Date(
        'Prescription Date'), 'get_report_prescription_date')
    report_prescription_time = fields.Function(fields.Time(
        'Prescription Time'), 'get_report_prescription_time')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ('validated', 'Validated'),
        ], 'State', readonly=True, sort=False, states=STATES)

    @classmethod
    def __setup__(cls):
        super(PatientPrescriptionOrder, cls).__setup__()
        cls._order.insert(0, ('prescription_date', 'DESC'))
        cls._buttons.update({
            'create_prescription': {'invisible': Equal(Eval('state'), 'done')}
        })

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.patient.evaluation']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @classmethod
    def validate(cls, prescriptions):
        super(PatientPrescriptionOrder, cls).validate(prescriptions)
        for prescription in prescriptions:
            prescription.check_health_professional()
            prescription.check_prescription_warning()

    def check_health_professional(self):
        if not self.healthprof:
            raise UserWarning(gettext('health.msg_health_professional_warning'))

    def check_prescription_warning(self):
        if not self.prescription_warning_ack:
            raise UserError('health.msg_drug_pregnancy_warning')

    @staticmethod
    def default_healthprof():
        return Pool().get('health.professional').get_health_professional()

    # Method that makes the doctor to acknowledge if there is any
    # warning in the prescription

    @fields.depends('patient')
    def on_change_patient(self):
        preg_warning = False
        presc_warning_ack = True
        if self.patient:
            # Trigger the warning if the patient is at a childbearing age
            if (self.patient.childbearing_age):
                preg_warning = True
                presc_warning_ack = False

        self.prescription_warning_ack = presc_warning_ack
        self.pregnancy_warning = preg_warning

    @staticmethod
    def default_prescription_date():
        return datetime.now()

    @staticmethod
    def default_user():
        return Transaction().user

    @classmethod
    def default_state(cls):
        return 'draft'

    def get_report_prescription_date(self, name):
        Company = Pool().get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.prescription_date
        return datetime.astimezone(dt.replace(tzinfo=pytz.utc), timezone).date()

    def get_report_prescription_time(self, name):
        Company = Pool().get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.prescription_date
        return datetime.astimezone(dt.replace(tzinfo=pytz.utc), timezone).time()

    @classmethod
    def create(cls, vlist):
        config = Pool().get('health.configuration').get_config()

        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('prescription_id'):
                values['prescription_id'] = config.prescription_sequence.get()

        return super(PatientPrescriptionOrder, cls).create(vlist)

    @classmethod
    def copy(cls, prescriptions, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['prescription_id'] = None
        default['prescription_date'] = cls.default_prescription_date()
        return super(PatientPrescriptionOrder, cls).copy(
            prescriptions, default=default)

    @classmethod
    @ModelView.button
    def create_prescription(cls, prescriptions):
        # Change the state of the prescription to "Done"
        cls.write(prescriptions, {'state': 'done'})


class PrescriptionLine(ModelSQL, ModelView):
    'Prescription Line'
    __name__ = 'health.prescription.line'

    order = fields.Many2One('health.prescription.order', 'Prescription ID')
    review = fields.DateTime('Valid Until', help="Until this date, the patient \
        usually can ask for a refill / reorder of this medicament")
    quantity = fields.Integer('Units',
        help="Number of units of the medicament."
        " Example : 30 capsules of amoxicillin")
    refills = fields.Integer('Refills #')
    allow_substitution = fields.Boolean('Allow substitution')
    short_comment = fields.Char(
        'Comment', help='Short comment on the specific drug')
    prnt = fields.Boolean('Print',
        help='Check this box to print this line of the prescription.')
    add_to_history = fields.Boolean('Hist',
        help='Include this medication in the patient medication history')
    medicament = fields.Many2One(
        'health.medicament', 'Medicament',
        required=True, help='Prescribed Medicament')
    indication = fields.Many2One('health.pathology', 'Indication',
        help='Choose a disease for this medicament from the disease list. It'
        ' can be an existing disease of the patient or a prophylactic.')
    start_treatment = fields.DateTime('Start',
        help='Date of start of Treatment')
    end_treatment = fields.DateTime('End', help='Date of start of Treatment')
    dose = fields.Float('Dose',
        help='Amount of medication (eg, 250 mg) per dose')
    dose_unit = fields.Many2One(
        'health.dose.unit', 'dose unit',
        help='Unit of measure for the medication to be taken')
    route = fields.Many2One(
        'health.drug.route', 'Administration Route',
        help='Drug administration route code.')
    form = fields.Many2One(
        'health.drug.form', 'Form',
        help='Drug form, such as tablet or gel')
    qty = fields.Integer(
        'x',
        help='Quantity of units (eg, 2 capsules) of the medicament')
    common_dosage = fields.Many2One(
        'health.medication.dosage', 'Frequency',
        help='Common / standard dosage frequency for this medicament')
    admin_times = fields.Char(
        'Admin hours',
        help='Suggested administration hours. For example, at 08:00, 13:00'
        ' and 18:00 can be encoded like 08 13 18')
    frequency = fields.Integer(
        'Frequency',
        help='Time in between doses the patient must wait (ie, for 1 pill'
        ' each 8 hours, put here 8 and select \"hours\" in the unit field')
    frequency_unit = fields.Selection([
        (None, ''),
        ('seconds', 'seconds'),
        ('minutes', 'minutes'),
        ('hours', 'hours'),
        ('days', 'days'),
        ('weeks', 'weeks'),
        ('wr', 'when required'),
        ], 'unit', select=True, sort=False)
    frequency_prn = fields.Boolean('PRN', help='Use it as needed, pro re nata')
    duration = fields.Integer(
        'Treatment duration',
        help='Period that the patient must take the medication. in minutes,'
        ' hours, days, months, years or indefinately')
    duration_period = fields.Selection([
        (None, ''),
        ('minutes', 'minutes'),
        ('hours', 'hours'),
        ('days', 'days'),
        ('months', 'months'),
        ('years', 'years'),
        ('indefinite', 'indefinite'),
        ], 'Treatment period', sort=False,
        help='Period that the patient must take the medication in minutes,'
        ' hours, days, months, years or indefinately')
    infusion = fields.Boolean(
        'Infusion',
        help='Mark if the medication is in the form of infusion' \
        ' Intravenous, Gastrostomy tube, nasogastric, etc...' )
    infusion_rate = fields.Float('Rate',
        states={'invisible': Not(Bool(Eval('infusion')))})
    infusion_rate_units = fields.Selection([
        (None, ''),
        ('mlhr', 'mL/hour'),
        ], 'Unit Rate',
        states={'invisible': Not(Bool(Eval('infusion')))},
        select=True, sort=False)

    @staticmethod
    def default_qty():
        return 1

    @staticmethod
    def default_duration_period():
        return 'days'

    @staticmethod
    def default_frequency_unit():
        return 'hours'

    @staticmethod
    def default_quantity():
        return 1

    @staticmethod
    def default_prnt():
        return True

    @staticmethod
    def default_start_treatment():
        return datetime.now()

    @fields.depends('medicament')
    def on_change_medicament(self):
        if self.medicament:
            self.dose = self.medicament.strength
            self.dose_unit = self.medicament.unit
            self.form = self.medicament.form
            self.route = self.medicament.route

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]

        for values in vlist:
            if values.get('add_to_history'):

                Medication = Pool().get('health.patient.medication')
                med = []

                # Retrieve the patient ID from the prescription
                Prescs = Pool().get('health.prescription.order')
                patient = Prescs.browse([values['order']])[0].patient.id

                medicament = values['medicament']
                indication = values['indication']
                start_treatment = values.get('start_treatment')
                prescription = values['order']

                values = {
                    'name': patient,
                    'medicament': medicament,
                    'indication': indication,
                    'start_treatment': start_treatment,
                    'prescription': prescription
                    }

                # Add the medicament from the prescription
                med.append(values)
                Medication.create(med)

        return super(PrescriptionLine, cls).create(vlist)


class PatientECG(ModelSQL, ModelView):
    'Patient ECG'
    __name__ = 'health.patient.ecg'

    patient = fields.Many2One('health.patient', 'Patient', required=True)
    ecg_date = fields.DateTime('Date', required=True)
    lead = fields.Selection([
        (None, ''),
        ('i', 'I'),
        ('ii', 'II'),
        ('iii', 'III'),
        ('avf', 'aVF'),
        ('avr', 'aVR'),
        ('avl', 'aVL'),
        ('v1', 'V1'),
        ('v2', 'V2'),
        ('v3', 'V3'),
        ('v4', 'V4'),
        ('v5', 'V5'),
        ('v6', 'V6')],
        'Lead', sort=False)
    axis = fields.Selection([
        (None, ''),
        ('normal', 'Normal Axis'),
        ('left', 'Left deviation'),
        ('right', 'Right deviation'),
        ('extreme_right', 'Extreme right deviation')],
        'Axis', sort=False, required=True)
    rate = fields.Integer('Rate', required=True)
    rhythm = fields.Selection([
        (None, ''),
        ('regular', 'Regular'),
        ('irregular', 'Irregular')],
        'Rhythm', sort=False, required=True)
    pacemaker = fields.Selection([
        (None, ''),
        ('sa', 'Sinus Node'),
        ('av', 'Atrioventricular'),
        ('pk', 'Purkinje')
        ],
        'Pacemaker', sort=False, required=True)
    pr = fields.Integer('PR', help="Duration of PR interval in milliseconds")
    qrs = fields.Integer('QRS', help="Duration of QRS interval in milliseconds")
    qt = fields.Integer('QT', help="Duration of QT interval in milliseconds")
    st_segment = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('depressed', 'Depressed'),
        ('elevated', 'Elevated')],
        'ST Segment', sort=False, required=True)
    twave_inversion = fields.Boolean('T wave inversion')
    interpretation = fields.Char('Interpretation', required=True)
    ecg_strip = fields.Binary('ECG Strip')
    healthprof = fields.Many2One(
        'health.professional',
        'Health Prof', readonly=True,
        help='Health Professional who performed the ECG')
    institution = fields.Many2One('health.institution', 'Institution')

    @staticmethod
    def default_ecg_date():
        return datetime.now()

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_healthprof():
        return Pool().get('health.professional').get_health_professional()

    @classmethod
    def validate(cls, ecgs):
        super(PatientECG, cls).validate(ecgs)
        for ecg in ecgs:
            ecg.check_health_professional()

    def check_health_professional(self):
        if not self.healthprof:
            raise UserError(gettext('health_professional_warning'))

    # Return the ECG Interpretation with main components
    def get_rec_name(self, name):
        if self.patient:
            res = str(self.interpretation) + ' // Rate ' + str(self.rate)
        return res
